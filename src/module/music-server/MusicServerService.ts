import PageResult from "@/domain/PageResult";
import {MusicServerInfoItem, MusicServerListItem} from "@/domain/music-server";

export default interface MusicServerService {

    /**
     * 初始化数据
     */
    init(): Promise<void>;

    /**
     * 分页查询列表
     * @param num 页码，从1开始
     * @param size 每页大小
     * @param name 名称
     */
    page(num: number, size: number, name: string): Promise<PageResult<MusicServerListItem>>;

    /**
     * 根据ID查询歌曲详情
     * @param id 歌曲ID
     */
    info(id: string): Promise<MusicServerInfoItem>;

}
