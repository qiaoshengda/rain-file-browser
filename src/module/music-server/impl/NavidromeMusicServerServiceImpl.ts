import MusicServerService from "@/module/music-server/MusicServerService";
import {getDefaultMusicServerAuth, MusicServer, MusicServerAuth} from "@/entity/music-server";
import axios from "axios";
import {MusicServerInfoItem, MusicServerListItem} from "@/domain/music-server";
import PageResult from "@/domain/PageResult";
import Constant from "@/global/Constant";
import {getStrBySession, setStrBySession} from "@/utils/utools/DbStorageUtil";

interface UserInfo {
    id: string;
    isAdmin: boolean;
    name: string;
    subsonicSalt: string;
    subsonicToken: string;
    token: string;
    username: string;
}

interface PageListItem {
    playCount: number;
    playDate: string;
    rating: number;
    starred: boolean;
    starredAt: string;
    bookmarkPosition: number;
    id: string;
    path: string;
    title: string;
    album: string;
    artistId: string;
    artist: string;
    albumArtistId: string;
    albumArtist: string;
    albumId: string;
    hasCoverArt: boolean;
    trackNumber: number;
    discNumber: number;
    year: number;
    size: number;
    suffix: string;
    duration: number;
    bitRate: number;
    channels: number;
    genre: string;
    genres?: any;
    fullText: string;
    orderTitle: string;
    orderAlbumName: string;
    orderArtistName: string;
    orderAlbumArtistName: string;
    compilation: boolean;
    createdAt: string;
    updatedAt: string;
    lyrics?: string;
}

interface ScanStatusRsp {
    'subsonic-response': Subsonicresponse;
}

interface Subsonicresponse {
    status: string;
    version: string;
    type: string;
    serverVersion: string;
    scanStatus: ScanStatus;
}

interface ScanStatus {
    scanning: boolean;
    count: number;
    folderCount: number;
    lastScan: string;
}

export default class NavidromeMusicServerServiceImpl implements MusicServerService {

    private token: string = '';
    private subsonicSalt: string = '';
    private subsonicToken: string = '';

    private count: number = 0;
    private readonly auth: MusicServerAuth;
    private readonly nativeId: string = utools.getNativeId();

    constructor(data: MusicServer) {
        const nativeAuth = Object.assign(getDefaultMusicServerAuth(), data.nativeAuth[this.nativeId]);
        this.auth = {
            url: nativeAuth.url || data.auth.url,
            useToken: nativeAuth.useToken || data.auth.useToken,
            username: nativeAuth.username || data.auth.username,
            password: nativeAuth.password || data.auth.password,
            token: nativeAuth.token || data.auth.token,
        };
    }

    async init(): Promise<void> {
        if (this.auth.useToken) {
            return Promise.reject("Navidrome只支持基础认证！");
        }

        this.defaultGet();

        if (this.token === '') {
            await this.login();
        }

        this.defaultSet();

        await this.etScanStatus();
        return Promise.resolve();
    }

    defaultGet() {
        const key = this.auth.url + this.auth.username + this.auth.password;

        this.token = getStrBySession(key + ':' + 'token');
        this.subsonicSalt = getStrBySession(key + ':' + 'subsonicSalt');
        this.subsonicToken = getStrBySession(key + ':' + 'subsonicToken');

    }

    defaultSet() {
        const key = this.auth.url + this.auth.username + this.auth.password;

        setStrBySession(key + ':' + 'token', this.token);
        setStrBySession(key + ':' + 'subsonicSalt', this.subsonicSalt);
        setStrBySession(key + ':' + 'subsonicToken', this.subsonicToken);
    }

    async login(): Promise<void> {
        const rsp = await axios<UserInfo>({
            baseURL: this.auth.url,
            url: '/auth/login',
            method: 'POST',
            data: {username: this.auth.username, password: this.auth.password}
        });
        this.token = rsp.data.token;
        this.subsonicSalt = rsp.data.subsonicSalt;
        this.subsonicToken = rsp.data.subsonicToken;
    }

    // 获取基础数据
    async etScanStatus() {
        const rsp = await axios<ScanStatusRsp>({
            baseURL: this.auth.url,
            url: '/rest/getScanStatus',
            method: 'GET',
            headers: {
                'X-Nd-Client-Unique-Id': this.nativeId,
                'X-Nd-Authorization': 'Bearer ' + this.token
            },
            params: {
                u: 'admin',
                t: this.subsonicToken,
                s: this.subsonicSalt,
                f: 'json',
                v: Constant.version,
                c: 'NavidromeUI',
            }
        });
        this.count = rsp.data['subsonic-response'].scanStatus.count;
    }

    async page(num: number, size: number, name: string): Promise<PageResult<MusicServerListItem>> {
        if ((num - 1) * size > this.count) {
            return Promise.resolve({num, size, total: this.count, records: []});
        }
        const rsp = await axios<Array<PageListItem>>({
            baseURL: this.auth.url,
            url: '/api/song',
            method: 'GET',
            headers: {
                'X-Nd-Client-Unique-Id': this.nativeId,
                'X-Nd-Authorization': 'Bearer ' + this.token
            },
            params: {
                _end: num * size,
                _order: 'ASC',
                _sort: 'title',
                _start: (num - 1) * size,
                title: name === '' ? undefined : name
            }
        });

        return Promise.resolve({
            num,
            size,
            total: this.count,
            records: rsp.data.map(e => ({
                id: e.id,
                url: this.auth.url + '/rest/stream' +
                    '?u=admin' +
                    '&t=' + this.subsonicToken +
                    '&s=' + this.subsonicSalt +
                    '&f=json' +
                    '&v=' + Constant.version +
                    '&c=' + Constant.id +
                    '&id=' + e.id +
                    '&_=' + new Date().getTime(),
                cover: this.auth.url + '/rest/getCoverArt' +
                    '?u=admin' +
                    '&t=' + this.subsonicToken +
                    '&s=' + this.subsonicSalt +
                    '&f=json' +
                    '&v=' + Constant.version +
                    '&c=' + Constant.id +
                    '&id=' + e.id +
                    '&_=' + new Date() +
                    '&size=300',
                title: e.title,
                album: e.albumId,
                artist: e.artist,
                suffix: e.suffix,
                duration: e.duration
            }))
        });
    }

    async info(id: string): Promise<MusicServerInfoItem> {
        const rsp = await axios<PageListItem>({
            baseURL: this.auth.url,
            url: '/api/song/' + id,
            method: 'GET',
            headers: {
                'X-Nd-Client-Unique-Id': this.nativeId,
                'X-Nd-Authorization': 'Bearer ' + this.token
            },
        });
        return Promise.resolve({
            url: this.auth.url + '/rest/stream' +
                '?u=admin' +
                '&t=' + this.subsonicToken +
                '&s=' + this.subsonicSalt +
                '&f=json' +
                '&v=' + Constant.version +
                '&c=' + Constant.id +
                '&id=' + id +
                '&_=' + new Date().getTime(),
            cover: this.auth.url + '/rest/getCoverArt' +
                '?u=admin' +
                '&t=' + this.subsonicToken +
                '&s=' + this.subsonicSalt +
                '&f=json' +
                '&v=' + Constant.version +
                '&c=' + Constant.id +
                '&id=' + id +
                '&_=' + new Date() +
                '&size=300',
            lyrics: rsp.data.lyrics
        });
    }

}
