import MusicServerService from "@/module/music-server/MusicServerService";
import {MusicServerInfoItem, MusicServerListItem} from "@/domain/music-server";
import PageResult from "@/domain/PageResult";

export default class DefaultMusicServerService implements MusicServerService {
    info(id: string): Promise<MusicServerInfoItem> {
        return Promise.reject("未实现");
    }

    init(): Promise<void> {
        return Promise.reject("未实现");
    }

    page(num: number, size: number, name: string): Promise<PageResult<MusicServerListItem>> {
        return Promise.reject("未实现");
    }

}
