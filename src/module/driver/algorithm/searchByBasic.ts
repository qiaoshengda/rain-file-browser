import Driver from "@/module/driver/Driver";
import FileSearch from "@/domain/file/FileSearch";
import {SearchCallback, UploadResult} from "@/domain/http/HttpUpload";
import FileListItem from "@/domain/file/FileListItem";


interface SearchData {
    close: boolean;
}

/**
 * 原生搜索算法
 * @param driver 驱动
 * @param search 搜索项
 * @param callback 回调
 * @return 结果
 */
export function searchByBasic(driver: Driver, search: Partial<FileSearch>, callback: SearchCallback): UploadResult {
    const parent = search.parent || '/';
    const data: SearchData = {
        close: false
    };
    // 递归搜索
    searchWrap(parent, driver, search, callback, data)
        .then(() => callback.onSuccess())
        .catch(e => callback.onError(e));
    return {
        abort: () => data.close = true
    };
}

async function searchWrap(path: string, driver: Driver, search: Partial<FileSearch>, callback: SearchCallback, data: SearchData) {
    const items = await driver.list(path);
    for (let item of items) {
        let target: FileListItem | null = null;
        if (typeof search.keyword !== 'undefined') {
            if (search.keyword !== '' && item.name.indexOf(search.keyword) == -1) {
                continue;
            }
        }
        if (search.extras && search.extras.length > 0) {
            if (search.extras.indexOf(item.extra) > -1 && !item.folder) {
                target = item
            }
        } else {
            if (search.dir === 'only') {
                if (item.folder) {
                    target = item;
                }
            } else if (search.dir === 'exclude') {
                if (!item.folder) {
                    target = item;
                }
            } else {
                target = item;
            }
        }
        if (target) {
            callback.onProgress([{
                ...item,
                parent: path
            }])
        }
        if (data.close) {
            return;
        }
        if (item.folder) {
            // 文件夹，继续递归
            await searchWrap(item.path, driver, search, callback, data);
        }
    }
}
