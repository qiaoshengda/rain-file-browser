import {createClient, FileStat, ResponseDataDetailed, WebDAVClient} from "webdav";
import {parseFileExtra} from "@/utils/FieldUtil";
import Driver from "../Driver";
import BaseAuth from "@/entity/auth/BaseAuth";
import FileListItem from "@/domain/file/FileListItem";
import {SearchCallback, UploadCallback, UploadResult} from "@/domain/http/HttpUpload";
import MessageUtil from "@/utils/MessageUtil";
import FileSearch from "@/domain/file/FileSearch";
import {searchByBasic} from "@/module/driver/algorithm/searchByBasic";
import FileDownloadLink from "@/domain/file/FileDownloadLink";

export default class WebDavDriver implements Driver {

    private readonly client: WebDAVClient;

    private readonly storage: BaseAuth;

    constructor(storage: BaseAuth) {
        this.storage = storage;
        this.client = createClient(storage.url, {
            username: storage.username,
            password: storage.password,
        })
    }

    init(): Promise<void> {
        return Promise.resolve();
    }

    getFileDownloadLink(item: FileListItem): Promise<string> {
        return Promise.resolve(this.client.getFileDownloadLink(item.path));
    }

    copyFile(path: string, destination: string): Promise<void> {
        return this.client.copyFile(path, destination);
    }

    moveFile(path: string, destinationPath: string): Promise<void> {
        return this.client.moveFile(path, destinationPath);
    }

    async list(path: string): Promise<FileListItem[]> {
        let files = await this.client.getDirectoryContents(path);
        // @ts-ignore
        if (files['data']) {
            files = files as ResponseDataDetailed<Array<FileStat>>
            return Promise.resolve(files.data.map(e => {
                return {
                    name: e.basename,
                    sha: '',
                    path: e.filename,
                    folder: e.type === 'directory',
                    updateTime: e.lastmod,
                    size: e.size,
                    cover: '',
                    extra: parseFileExtra(e.basename, e.type === 'directory')
                }
            }));
        } else {
            files = files as Array<FileStat>;
            return Promise.resolve(files.map(e => {
                return {
                    name: e.basename,
                    sha: '',
                    path: e.filename,
                    folder: e.type === 'directory',
                    updateTime: e.lastmod,
                    size: e.size,
                    cover: '',
                    extra: parseFileExtra(e.basename, e.type === 'directory')
                }
            }));
        }
    }

    mkdir(name: string, path: string): Promise<void> {
        return this.client.createDirectory(path + '/' + name);
    }

    rename(item: FileListItem, newName: string, path: string): Promise<void> {
        return this.client.moveFile(`${path}/${item.name}`, `${path}/${newName}`);
    }

    touch(path: string, name: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.client.putFileContents(path + '/' + name, '', {
                overwrite: true,
            }).then(() => resolve())
                .catch(e => reject(e));
        });
    }

    async delete(items: Array<FileListItem>): Promise<void> {
        for (let item of items) {
            await this.client.deleteFile(item.path)
        }
        return Promise.resolve();
    }

    readText(item: FileListItem, progress?: (current: number, total: number) => void): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            (this.client.getFileContents(item.path, {
                onDownloadProgress: (e) => {
                    if (progress) {
                        progress(e.loaded, e.total)
                    }
                }
            }) as Promise<ArrayBuffer>)
                .then((content: ArrayBuffer) => {
                    let fileReader = new FileReader();
                    fileReader.readAsText(new Blob([content]), 'utf-8');
                    fileReader.onload = function (evt: ProgressEvent<FileReader>) {
                        if (evt.target) {
                            resolve(evt.target.result as string);
                        }
                    }

                }).catch(e => reject(e));
        });
    }

    writeText(item: FileListItem, content: string, progress?: (current: number, total: number) => void): Promise<boolean> {
        return this.client.putFileContents(item.path, content, {
            overwrite: true,
            onUploadProgress: (e) => {
                if (progress) {
                    progress(e.loaded, e.total);
                }
            }
        });
    }

    upload(file: File, targetPath: string, callback: UploadCallback): UploadResult {
        file.arrayBuffer()
            .then(buffer => {
                this.client.putFileContents(targetPath, buffer, {
                    overwrite: false,
                    onUploadProgress: (e) => {
                        callback.onProgress(e.loaded);
                    }
                }).then(() => callback.onSuccess())
                    .catch(e => callback.onError(e));
            }).catch(e => callback.onError(e));
        return {
            abort: () => MessageUtil.warning("WebDAV不支持取消上传")
        }
    }

    download(source: FileListItem, targetName: string, targetPath: string, progress: (current: number, total: number) => void): Promise<string> {
        return window.preload.fs.download({
            baseURL: this.storage.url,
            url: source.path,
            method: 'GET',
            auth: {
                username: this.storage.username,
                password: this.storage.password
            }
        }, targetName, targetPath, progress);
    }

    destroy(): Promise<void> {
        return Promise.resolve();
    }

    search(search: FileSearch, callback: SearchCallback): UploadResult {
        return searchByBasic(this, search, callback);
    }

    getFileDownloadLinks(items: FileListItem[]): Promise<FileDownloadLink[]> {
        return Promise.resolve(items.map(item => ({
            host: this.storage.url,
            path: this.client.getFileDownloadLink(item.path).replace(this.storage.url, "")
        })));
    }


}
