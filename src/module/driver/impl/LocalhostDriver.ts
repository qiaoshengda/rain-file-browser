import Driver from "@/module/driver/Driver";
import FileListItem from "@/domain/file/FileListItem";
import FileSearch from "@/domain/file/FileSearch";
import {SearchCallback, UploadCallback, UploadResult} from "@/domain/http/HttpUpload";
import BaseAuth from "@/entity/auth/BaseAuth";
import FileDownloadLink from "@/domain/file/FileDownloadLink";
import {parseFileExtra, pathJoin} from "@/utils/FieldUtil";
import {searchByBasic} from "@/module/driver/algorithm/searchByBasic";

export default class LocalhostDriver implements Driver {

    private readonly path: string;

    constructor(auth: BaseAuth) {
        this.path = auth.url;
    }

    copyFile(path: string, destination: string): Promise<void> {
        return window.preload.file.copyFileWrap(this.path, path, destination);
    }

    delete(items: Array<FileListItem>): Promise<void> {
        return Promise.resolve(undefined);
    }

    destroy(): Promise<void> {
        return Promise.resolve(undefined);
    }

    download(source: FileListItem, targetName: string, targetPath: string, progress: (current: number, total: number) => void): Promise<string> {
        return Promise.resolve("");
    }

    getFileDownloadLink(item: Pick<FileListItem, "path" | "extra">): Promise<string> {
        return Promise.resolve(pathJoin(this.path, item.path));
    }

    getFileDownloadLinks(items: Array<FileListItem>): Promise<Array<FileDownloadLink>> {
        return Promise.resolve(items.map(item => ({
            host: '',
            path: pathJoin(this.path, item.path)
        })));
    }

    init(): Promise<void> {
        return Promise.resolve();
    }

    async list(path: string): Promise<Array<FileListItem>> {
        const items = await window.preload.file.list(this.path, path);
        const fileItems = new Array<FileListItem>();
        for (let item of items) {
            fileItems.push({
                ...item,
                extra: parseFileExtra(item.path),
                cover: '',
            })
        }
        return Promise.resolve<Array<FileListItem>>(fileItems);
    }

    mkdir(name: string, path: string): Promise<void> {
        return Promise.resolve(undefined);
    }

    moveFile(path: string, destinationPath: string): Promise<void> {
        return Promise.resolve(undefined);
    }

    readText(item: FileListItem, progress?: (current: number, total: number) => void): Promise<string> {
        return Promise.resolve("");
    }

    rename(item: FileListItem, newName: string, path: string): Promise<void> {
        return Promise.resolve(undefined);
    }

    search(search: Partial<FileSearch>, callback: SearchCallback): UploadResult {
        return searchByBasic(this, search, callback);
    }

    touch(path: string, name: string): Promise<void> {
        return Promise.resolve(undefined);
    }

    upload(file: File, targetPath: string, callback: UploadCallback): UploadResult {
        return {
            abort: () => {
            }
        };
    }

    writeText(item: FileListItem, content: string, progress?: (current: number, total: number) => void): Promise<boolean> {
        return Promise.resolve(false);
    }

}
