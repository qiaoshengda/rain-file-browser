import HttpRequest from "@/domain/http/HttpRequest";
import http from "@/plugins/http";
import {parseFileExtra} from "@/utils/FieldUtil";
import Driver from "../Driver";
import StorageByAlistV3 from "@/entity/WebStorage/info/StorageByAlistV3";
import FileListItem from "@/domain/file/FileListItem";
import axios from "axios";
import {SearchCallback, UploadCallback, UploadResult} from "@/domain/http/HttpUpload";
import FileSearch from "@/domain/file/FileSearch";
import {getCurrentStoragePrefix} from "@/store/db/StorageStore";
import StorageTypeEnum from "@/enumeration/StorageTypeEnum";
import {searchByBasic} from "@/module/driver/algorithm/searchByBasic";
import MessageUtil from "@/utils/MessageUtil";
import {Data, FileData, FileInfo, FileItem, Result, Setting} from "@/domain/file/AlistDomain";
import FileDownloadLink from "@/domain/file/FileDownloadLink";

export default class AlistV3Driver implements Driver {

    private readonly id: number;
    private authorization: string = "";
    private baseURL;
    private setting: Setting | null;

    private tip = false;

    /**
     * 初始化
     *
     * @param id 存储ID
     * @param storage 存储信息
     */
    constructor(id: number, storage: StorageByAlistV3) {
        this.id = id;
        this.baseURL = storage.url;
        this.authorization = storage.token;
        if (this.baseURL === '') {
            throw new Error("alist链接不能为空");
        }
        this.setting = null;
    }

    async init(): Promise<void> {
        // 获取单独配置
        const res = await utools.db.promises.get(getCurrentStoragePrefix(StorageTypeEnum.ALIST_V3, this.id) + utools.getNativeId())
        if (res) {
            const native = res.value as StorageByAlistV3;
            this.baseURL = native.url || this.baseURL;
            this.authorization = native.token || this.authorization;
        }
        // 获取服务器设置
        const rsp = await axios<Result<Setting>>(this.buildHttpRequest({
            url: '/api/public/settings',
            method: 'GET'
        }));
        this.setting = rsp.data.data;
        return Promise.resolve();
    }

    destroy(): Promise<void> {
        return Promise.resolve();
    }

    private buildHttpRequest(config: HttpRequest<any>): HttpRequest<any> {
        // 基础URL
        config.baseURL = this.baseURL;
        // 认证字符串
        config.headers = Object.assign({}, {
            Authorization: this.authorization
        }, config.headers);
        return config
    }

    private buildHttp<T, D = any>(config: HttpRequest<D>): Promise<Result<T>> {
        return new Promise<Result<T>>((resolve, reject) => {
            http<Result<T>, D>(this.buildHttpRequest(config)).then(rsp => {
                let result = rsp.data;
                if (result.code !== 200) {
                    reject(result.message);
                    return;
                }
                resolve(result);
            });
        });
    }

    list(path: string): Promise<FileListItem[]> {
        return new Promise<FileListItem[]>((resolve, reject) => {
            this.buildHttp<FileData>({
                url: '/api/fs/list',
                method: 'POST',
                data: {
                    path,
                    password: "",
                    page: 1,
                    per_page: 0,
                    refresh: false
                },
            }).then(result => {
                if (path === '/') {
                    path = '';
                }
                let items = new Array<FileListItem>();
                let content = result.data.content as Array<FileItem>;
                if (content) {
                    for (let temp of content) {
                        items.push({
                            name: temp.name,
                            size: temp.size,
                            extra: parseFileExtra(temp.name),
                            folder: temp.is_dir,
                            updateTime: temp.modified,
                            path: path + '/' + temp.name,
                            cover: temp.thumb,
                            expands: {
                                sign: temp.sign
                            }
                        });
                    }
                }
                resolve(items);
            }).catch(e => reject(e));
        })
    }

    mkdir(name: string, path: string): Promise<void> {
        if (this.authorization === '') {
            return Promise.reject("没有token无法操作");
        }
        return new Promise<void>((resolve, reject) => {
            this.buildHttp({
                url: '/api/fs/mkdir',
                method: 'POST',
                data: {
                    path: path + (path.endsWith('/') ? '' : '/') + name
                }
            }).then(() => resolve())
                .catch(e => reject(e));
        });
    }

    rename(item: FileListItem, newName: string, path: string): Promise<void> {
        if (this.authorization === '') {
            return Promise.reject("没有token无法操作");
        }
        return new Promise<void>((resolve, reject) => {
            this.buildHttp({
                url: '/api/fs/rename',
                method: 'POST',
                data: {
                    name: newName,
                    path: path + (path.endsWith('/') ? '' : '/') + item.name
                }
            }).then(() => resolve())
                .catch(e => reject(e));
        });
    }

    delete(items: Array<FileListItem>): Promise<void> {
        if (this.authorization === '') {
            return Promise.reject("没有token无法操作");
        }
        const path = items[0].path;
        let nameIndex = path.lastIndexOf("/");
        let dir = path.substring(0, nameIndex);
        return new Promise<void>((resolve, reject) => {
            this.buildHttp({
                url: '/api/fs/remove',
                method: 'POST',
                data: {
                    dir,
                    names: items.map(item => item.path.substring(nameIndex + 1))
                }
            }).then(() => resolve())
                .catch(e => reject(e));
        });
    }

    copyFile(path: string, destinationPath: string): Promise<void> {
        if (this.authorization === '') {
            return Promise.reject("没有token无法操作");
        }
        return new Promise<void>((resolve, reject) => {
            let nameIndex = path.lastIndexOf("/");
            let src_dir = path.substring(0, nameIndex);
            let name = path.substring(nameIndex + 1);
            let dstDirIndex = destinationPath.lastIndexOf("/");
            let dst_dir = destinationPath.substring(0, dstDirIndex);
            this.buildHttp({
                url: '/api/fs/copy',
                method: 'POST',
                data: {
                    src_dir,
                    dst_dir,
                    names: [name]
                }
            }).then(() => resolve())
                .catch(e => reject(e));
        });
    }

    moveFile(path: string, destinationPath: string): Promise<void> {
        if (this.authorization === '') {
            return Promise.reject("没有token无法操作");
        }
        return new Promise<void>((resolve, reject) => {
            let nameIndex = path.lastIndexOf("/");
            let src_dir = path.substring(0, nameIndex);
            let name = path.substring(nameIndex + 1);
            let dstDirIndex = destinationPath.lastIndexOf("/");
            let dst_dir = destinationPath.substring(0, dstDirIndex);
            this.buildHttp({
                url: '/api/fs/move',
                method: 'POST',
                data: {
                    src_dir,
                    dst_dir,
                    names: [name]
                }
            }).then(() => resolve())
                .catch(e => reject(e));
        });
    }

    touch(path: string, name: string): Promise<void> {
        if (this.authorization === '') {
            return Promise.reject("没有token无法操作");
        }
        return new Promise<void>((resolve, reject) => {
            this.buildHttp({
                url: '/api/fs/put',
                method: 'PUT',
                headers: {
                    "File-Path": encodeURIComponent(path + '/' + name)
                }
            }).then(() => {
                resolve();
            }).catch(e => reject(e));
        });
    }

    readText(item: FileListItem, progress?: ((current: number, total: number) => void) | undefined): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.buildHttp<FileInfo>({
                url: '/api/fs/get',
                method: 'POST',
                data: {
                    path: item.path,
                    password: ''
                },
            }).then(result => {
                let url = result.data.raw_url as string;
                if (this.baseURL.startsWith("https") && !url.startsWith("https")) {
                    // 如果站点是https，但是链接不是
                    url = url.replace('http', 'https');
                }
                http<string>({
                    url,
                    method: 'GET',
                    onDownloadProgress(progressEvent) {
                        if (progress) {
                            progress(progressEvent.rate || 0, progressEvent.total || 1);
                        }
                    },
                    responseType: 'text',
                    headers: {
                        Authorization: this.authorization
                    }
                }).then(rsp => {
                    resolve(rsp.data);
                }).catch(e => reject(e));
            }).catch(e => reject(e));
        })
    }

    writeText(item: FileListItem, content: string, progress?: ((current: number, total: number) => void) | undefined): Promise<boolean> {
        if (this.authorization === '') {
            return Promise.reject("没有token无法操作");
        }
        return new Promise<boolean>((resolve, reject) => {
            this.buildHttp({
                url: '/api/fs/put',
                method: 'PUT',
                data: content,
                headers: {
                    "File-Path": encodeURIComponent(item.path),
                    "Content-Type": "text/plain"
                },
                onUploadProgress: event => {
                    if (progress) {
                        progress(event.rate || 0, event.total || 1);
                    }
                }
            }).then(() => {
                resolve(true);
            }).catch(e => reject(e));
        });
    }

    upload(file: File, targetPath: string, callback: UploadCallback): UploadResult {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        const formData = new FormData();
        formData.append('file', file);

        axios({
            ...this.buildHttpRequest({
                url: '/api/fs/form',
                method: 'PUT',
                headers: {
                    "File-Path": encodeURIComponent(targetPath)
                },
            }),
            onUploadProgress: event => {
                callback.onProgress(event.progress || 0);
            },
            data: formData,
            cancelToken: source.token
        }).then(() => callback.onSuccess())
            .catch(e => callback.onError(e));

        return {abort: () => source.cancel()};
    }

    download(source: FileListItem, targetName: string, targetPath: string, progress: (current: number, total: number) => void): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.buildHttp<FileInfo>({
                url: '/api/fs/get',
                method: 'POST',
                data: {
                    path: source.path,
                    password: ''
                },
            }).then(result => {
                let url = result.data.raw_url as string;
                if (this.baseURL.startsWith("https") && !url.startsWith("https")) {
                    // 如果站点是https，但是链接不是
                    url = url.replace('http', 'https');
                }
                window.preload.fs.download(this.buildHttpRequest({
                    url,
                    method: 'GET',
                    onDownloadProgress(progressEvent) {
                        if (progress) {
                            progress(progressEvent.rate || 0, progressEvent.total || 1);
                        }
                    },
                    responseType: 'arraybuffer',
                    headers: {
                        Authorization: this.authorization
                    }
                }), targetName, targetPath, progress)
                    .then(path => resolve(path))
                    .catch(e => reject(e));

            }).catch(e => reject(e));
        })

    }

    getFileDownloadLink(item: FileListItem): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.buildHttp<FileInfo>({
                url: '/api/fs/get',
                method: 'POST',
                data: {
                    path: item.path,
                    password: ''
                },
            }).then(result => {
                let url = result.data.raw_url as string;
                if (this.baseURL.startsWith("https") && !url.startsWith("https")) {
                    // 如果站点是https，但是链接不是
                    url = url.replace('http', 'https');
                }
                resolve(url);
            }).catch(e => reject(e))
        });
    }

    search(search: Partial<FileSearch>, callback: SearchCallback): UploadResult {
        if (this.setting && this.setting.allow_indexed) {
            return this._search(search, callback);
        } else {
            if (!this.tip) {
                MessageUtil.warning("检测到您的AList未开启索引，将使用遍历搜索");
                this.tip = true
            }
            return searchByBasic(this, search, callback);
        }
    }

    private _search(search: Partial<FileSearch>, callback: SearchCallback): UploadResult {
        let flag = {
            stop: false
        };
        const result = {
            abort: () => {
                flag.stop = true;
            }
        }
        this._searchWrap(search, callback, flag).then(() => callback.onSuccess())
            .catch(e => callback.onError(e))
        return result;

    }

    private async _searchWrap(search: Partial<FileSearch>, callback: SearchCallback, flag: { stop: boolean }) {
        let page = 1;
        const size = 100;
        let total = 1;

        while ((page - 1) * size < total) {
            if (flag.stop) {
                break;
            }
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            const rsp = await axios<Result<Data>>({
                ...this.buildHttpRequest({
                    url: '/api/fs/search',
                    method: 'POST',
                }),
                data: {
                    parent: search.parent || '/',
                    keywords: search.keyword,
                    page: page,
                    per_page: size,
                    password: ""
                },
                cancelToken: source.token
            })
            const result = rsp.data;
            callback.onProgress(result.data.content.map(e => ({
                path: e.parent + '/' + e.name,
                size: e.size,
                name: e.name,
                extra: parseFileExtra(e.name),
                folder: e.is_dir,
                cover: '',
                updateTime: null,
                parent: e.parent
            })).filter(e => {
                // 过滤
                if (search.extras && search.extras.length > 0) {
                    // 如果有类型过滤，就不包含文件夹
                    return !e.folder && search.extras.indexOf(e.extra) > 0;
                } else {
                    if (search.dir === 'only') {
                        return e.folder;
                    } else if (search.dir === 'exclude') {
                        return !e.folder;
                    } else {
                        return true;
                    }
                }
            }))
            total = result.data.total
            page += 1;
        }

    }

    getFileDownloadLinks(items: Array<FileListItem>): Promise<Array<FileDownloadLink>> {
        return Promise.resolve(items.map(e => {
            return {
                host: this.baseURL,
                path: `/d${e.path}${e.expands ? (e.expands.sign ? '?sign=' + e.expands.sign : '') : ''}`
            };
        }));
    }

}
