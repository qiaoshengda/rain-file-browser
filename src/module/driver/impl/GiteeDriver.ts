import HttpRequest from "@/domain/http/HttpRequest";
import http from "@/plugins/http";
import {parseFileExtra} from "@/utils/FieldUtil";
import MessageBoxUtil from "@/utils/MessageBoxUtil";
import {download} from "@/utils/BrowserUtil";
import Driver from "../Driver";
import StorageByGitee from "@/entity/WebStorage/info/StorageByGitee";
import FileListItem from "@/domain/file/FileListItem";
import axios from "axios";
import {SearchCallback, UploadCallback, UploadResult} from "@/domain/http/HttpUpload";
import FileSearch from "@/domain/file/FileSearch";
import {searchByBasic} from "@/module/driver/algorithm/searchByBasic";
import FileDownloadLink from "@/domain/file/FileDownloadLink";

export default class GiteeDriver implements Driver {

    private readonly baseURL = "https://gitee.com/api/v5";

    private readonly storage: StorageByGitee;

    constructor(storage: StorageByGitee) {
        this.storage = storage;
    }

    private buildRequest(config: HttpRequest): HttpRequest {
        config.baseURL = this.baseURL;
        config.params = {
            access_token: this.storage.accessToken
        };
        return config;
    }

    private Decode64(str: string): string {
        return decodeURIComponent(atob(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }

    private base64ToUint8Array(base64String: string): ArrayBuffer {
        const base64 = base64String.replace(/^data:image\/\w+;base64,/, "");
        const rawData = window.atob(base64);
        const outputArray = new Uint8Array(rawData.length);
        for (let i = 0; i < rawData.length; ++i) {
            outputArray[i] = rawData.charCodeAt(i);
        }
        return outputArray;
    }

    init(): Promise<void> {
        return Promise.resolve();
    }

    list(path: string): Promise<FileListItem[]> {
        return new Promise<FileListItem[]>((resolve, reject) => {
            let target = path;
            if (target !== '/') {
                target = target.substring(1);
            }
            http<Array<any>>(this.buildRequest({
                url: `/repos/${this.storage.owner}/${this.storage.repo}/contents/${encodeURIComponent(target)}`,
                method: 'GET',
                responseType: 'json'
            })).then(rsp => {
                let items = new Array<FileListItem>();
                let list = rsp.data;
                if (path === '/') {
                    path = '';
                }
                for (let temp of list) {
                    items.push({
                        name: temp.name,
                        extra: parseFileExtra(temp.name, temp.type === 'dir'),
                        folder: temp.type === 'dir',
                        path: path + '/' + temp.name,
                        size: temp.size || 0,
                        updateTime: '',
                        cover: '',
                        expands: {
                            sha: temp.sha
                        }
                    });
                }
                resolve(items);
            }).catch(e => reject(e));
        })
    }

    mkdir(name: string, path: string): Promise<void> {
        return Promise.reject("Gitee不支持创建文件夹");
    }

    rename(item: FileListItem, newName: string, path: string): Promise<void> {
        return Promise.reject("Gitee暂不支持重命名文件");
    }

    async delete(items: Array<FileListItem>): Promise<void> {
        for (let item of items) {
            await this._deleteOne(item);
        }
        return Promise.resolve();
    }

    async _deleteOne(item: FileListItem): Promise<void> {
        // 第一步，先输入提交信息
        let value = await MessageBoxUtil.prompt("请输入提交信息", "删除文件", {
            inputValue: "听雨文件浏览器删除文件",
            confirmButtonText: "提交"
        });
        if (value.trim() === '') {
            throw new Error("提交信息不能为空！");
        }
        // 完善目录
        let target = item.path;
        if (target !== '/') {
            target = target.substring(1);
        }
        // 判断是新增还是更新
        if (!item.expands || !item.expands.sha) {
            return Promise.reject("没有sha，无法删除");
        }
        // 修改
        await http<any>(this.buildRequest({
            url: `/repos/${this.storage.owner}/${this.storage.repo}/contents/${encodeURIComponent(target)}`,
            method: 'DELETE',
            data: {
                message: value,
                sha: item.expands.sha
            }
        }));
        return Promise.resolve();
    }

    copyFile(path: string, destination: string): Promise<void> {
        return Promise.reject("Gitee暂不支持复制文件");
    }

    moveFile(path: string, destinationPath: string): Promise<void> {
        return Promise.reject("Gitee暂不支持移动文件");
    }

    readText(item: FileListItem, progress?: ((current: number, total: number) => void) | undefined): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            if (!item.expands) {
                reject("拓展信息不存在");
                return;
            }
            http<any>(this.buildRequest({
                url: `/repos/${this.storage.owner}/${this.storage.repo}/git/blobs/${item.expands.sha}`,
                method: 'GET',
            })).then(rsp => {
                let data = rsp.data;
                if (data.encoding === 'base64') {
                    resolve(this.Decode64(data.content));
                } else {
                    reject(`编码格式【${data.encoding}】未知`)
                }
            })
        });
    }

    async writeText(item: Pick<FileListItem, 'path' | 'expands'>, content: string, progress?: ((current: number, total: number) => void) | undefined): Promise<boolean> {
        // 第一步，先输入提交信息
        let value = await MessageBoxUtil.prompt("请输入提交信息", "写入文件", {
            inputValue: "听雨文件浏览器写入文件",
            confirmButtonText: "提交"
        });
        if (value.trim() === '') {
            throw new Error("提交信息不能为空！");
        }
        // 完善目录
        let target = item.path;
        if (target !== '/') {
            target = target.substring(1);
        }
        // 判断是新增还是更新
        if (item.expands && item.expands.sha) {
            // 修改
            await http<any>(this.buildRequest({
                url: `/repos/${this.storage.owner}/${this.storage.repo}/contents/${encodeURIComponent(target)}`,
                method: 'PUT',
                data: {
                    // @ts-ignore
                    content: Buffer.from(content).toString('base64'),
                    message: value,
                    sha: item.expands.sha
                }
            }));
            return Promise.resolve(true);
        } else {
            // 新增
            await http<any>(this.buildRequest({
                url: `/repos/${this.storage.owner}/${this.storage.repo}/contents/${encodeURIComponent(target)}`,
                method: 'POST',
                data: {
                    // @ts-ignore
                    content: Buffer.from(content).toString('base64'),
                    message: value
                }
            }));
            return Promise.resolve(true);
        }
    }

    upload(file: File, targetPath: string, callback: UploadCallback): UploadResult {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        // 第一步，先输入提交信息
        MessageBoxUtil.prompt("请输入提交信息", "写入文件", {
            inputValue: "听雨文件浏览器写入文件",
            confirmButtonText: "提交"
        }).then(value =>  {
            if (value.trim() === '') {
                throw new Error("提交信息不能为空！");
            }
            // 完善目录
            let target = targetPath;
            if (target !== '/') {
                target = target.substring(1);
            }

            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                let content = reader.result as string;
                // 新增
                axios<any>({
                    ...this.buildRequest({
                        url: `/repos/${this.storage.owner}/${this.storage.repo}/contents/${encodeURIComponent(target)}`,
                        method: 'POST',
                        data: {
                            content: content.replace(/^data:image\/\w+;base64,/, ""),
                            message: value
                        },
                        onUploadProgress: event => {
                            callback.onProgress(event.progress || 0)
                        },

                    }),
                    cancelToken: source.token
                }).then(() => callback.onSuccess())
                    .catch(e => callback.onError(e));
            };
            reader.onerror = callback.onError;
        }).catch(e => callback.onError(e));
        return {abort: () => source.cancel()};

    }

    download(source: FileListItem, targetName: string, targetPath: string, progress: (current: number, total: number) => void): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            if (!source.expands) {
                reject("拓展信息不存在");
                return;
            }
            http<any>(this.buildRequest({
                url: `/repos/${this.storage.owner}/${this.storage.repo}/git/blobs/${source.expands.sha}`,
                method: 'GET',
            })).then(rsp => {
                let data = rsp.data;
                if (data.encoding === 'base64') {
                    download(this.base64ToUint8Array(data.content), source.name, "base64")
                    resolve("");
                } else {
                    reject(`编码格式【${data.encoding}】未知`);
                }
            });
        })
    }

    getFileDownloadLink(item: FileListItem): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            let target = item.path;
            if (target !== '/') {
                target = target.substring(1);
            }
            http<any>(this.buildRequest({
                url: `/repos/${this.storage.owner}/${this.storage.repo}/contents/${encodeURIComponent(target)}`,
                method: 'GET'
            })).then(rsp => {
                resolve(rsp.data.download_url);
            }).catch(e => reject(e));
        })
    }

    destroy(): Promise<void> {
        return Promise.resolve();
    }

    async touch(path: string, name: string): Promise<void> {
        await this.writeText({
            path: path + '/' + name
        }, "");
    }

    search(search: FileSearch, callback: SearchCallback): UploadResult {
        return searchByBasic(this, search, callback);
    }

    async getFileDownloadLinks(items: Array<FileListItem>): Promise<Array<FileDownloadLink>> {
        let links = new Array<FileDownloadLink>();
        for (let item of items) {
            links.push({
                host: this.baseURL,
                path: await this.getFileDownloadLink(item)
            });
        }
        return Promise.resolve(links);
    }

}
