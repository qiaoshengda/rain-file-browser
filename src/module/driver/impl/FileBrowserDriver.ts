import Driver from "@/module/driver/Driver";
import FileListItem from "@/domain/file/FileListItem";
import HttpRequest from "@/domain/http/HttpRequest";
import BaseAuth from "@/entity/auth/BaseAuth";
import axios from "axios";
import {useSyncSettingStore} from "@/store/db/SyncSettingStore";
import {SearchCallback, UploadCallback, UploadResult} from "@/domain/http/HttpUpload";
import FileSearch from "@/domain/file/FileSearch";
import {parseFileExtra} from "@/utils/FieldUtil";
import {FileBrowserResource, Item, SearchItem} from "@/domain/file/FileBrowserDomain";
import FileDownloadLink from "@/domain/file/FileDownloadLink";


export default class FileBrowserDriver implements Driver {

    private readonly storage: BaseAuth;
    private token: string = '';

    constructor(storage: BaseAuth) {
        this.storage = storage;
    }

    private buildHttpRequest<D = any>(config: HttpRequest<D>): HttpRequest<D> {
        return {
            baseURL: this.storage.url,
            ...config,
            headers: {
                ...config.headers,
                'Content-Type': 'application/json',
                'X-Auth': this.token
            }
        };
    }

    private execute<T>(config: HttpRequest): Promise<T> {
        return axios<T>(this.buildHttpRequest(config))
            .then(e => e.data);
    }

    init(): Promise<void> {
        return this.execute<string>({
            url: '/api/login',
            method: 'POST',
            data: {
                username: this.storage.username,
                password: this.storage.password,
                recaptcha: ""
            }
        }).then(token => {
            this.token = token
        });
    }

    async list(path: string): Promise<Array<FileListItem>> {
        // /api/resources/
        const res = await this.execute<FileBrowserResource>({
            url: '/api/resources' + path,
            method: 'GET',
        })
        return Promise.resolve(res.items.map(item => {
            let extra = item.extension === '' ? '' : item.extension.substring(1);
            let cover = ''
            if (useSyncSettingStore().imageExtra.includes(extra)) {
                cover = this.buildImagePreviewUrl(item.path);
            }
            return {
                name: item.name,
                extra,
                folder: item.isDir,
                path: item.path,
                updateTime: item.modified,
                size: item.size,
                cover,
            }
        }));
    }


    async copyFile(path: string, destination: string): Promise<void> {
        await this.execute<void>({
            url: '/api/resources' + path,
            method: 'PATCH',
            params: {
                action: 'copy',
                destination,
                override: false,
                rename: false
            },
        });
        return Promise.resolve();
    }

    async delete(items: Array<FileListItem>): Promise<void> {
        for (let item of items) {
            await this._deleteOne(item);
        }
        return Promise.resolve();
    }

    private async _deleteOne(item: FileListItem): Promise<void> {
        const path = item.path;
        await this.execute<void>({
            url: '/api/resources' + path,
            method: 'DELETE',
        })
        return Promise.resolve();
    }

    async moveFile(path: string, destinationPath: string): Promise<void> {
        await this.execute<void>({
            url: '/api/resources' + path,
            method: 'PATCH',
            params: {
                action: 'rename',
                destination: destinationPath,
                override: false,
                rename: false
            },
        });
        return Promise.resolve();
    }

    async mkdir(name: string, path: string): Promise<void> {
        await this.execute<void>({
            url: '/api/resources' + path + '/' + name + '/',
            params: {
                override: false
            },
            method: 'POST'
        })
        return Promise.resolve(undefined);
    }

    async touch(path: string, name: string): Promise<void> {
        await this.execute<void>({
            url: '/api/resources' + path + '/' + name,
            method: 'POST',
            params: {
                override: false
            }
        })
        return Promise.resolve();
    }

    async rename(item: FileListItem, newName: string, path: string): Promise<void> {
        await this.execute<void>({
            url: '/api/resources' + item.path,
            method: 'PATCH',
            params: {
                action: 'rename',
                destination: path + '/' + newName,
                override: false,
                rename: false
            },
        });
        return Promise.resolve();
    }

    destroy(): Promise<void> {
        return Promise.resolve();
    }

    download(source: FileListItem, targetName: string, targetPath: string, progress: (current: number, total: number) => void): Promise<string> {
        return window.preload.fs.download(this.buildHttpRequest({
            url: this.buildImagePreviewUrl(source.path, "raw"),
            method: 'GET',
            onDownloadProgress(progressEvent) {
                if (progress) {
                    progress(progressEvent.rate || 0, progressEvent.total || 1);
                }
            },
            responseType: 'arraybuffer',
        }), targetName, targetPath, progress)
    }

    private buildImagePreviewUrl(path: string, level: string = 'preview/thumb'): string {
        return this.storage.url + this._buildImagePreviewUrlPath(path, level);
    }

    private _buildImagePreviewUrlPath(path: string, level: string = 'preview/thumb'): string {
        path = path.split("/").map(e => encodeURIComponent(e)).join("/");
        return `/api/${level}${path}?auth=${this.token}&inline=true&key=${new Date().getTime()}`
    }

    getFileDownloadLink(item: FileListItem): Promise<string> {
        if (useSyncSettingStore().imageExtra.includes(item.extra)) {
            return Promise.resolve(this.buildImagePreviewUrl(item.path, "preview/big"));
        } else {
            return Promise.resolve(this.buildImagePreviewUrl(item.path, "raw"));
        }
    }

    readText(item: FileListItem, progress?: (current: number, total: number) => void): Promise<string> {
        return this.execute<Item>({
            url: '/api/resources' + item.path,
            onDownloadProgress: event => {
                if (progress) {
                    progress(event.progress || 0, event.total || 1)
                }
            }
        }).then(e => e.content);
    }

    upload(file: File, targetPath: string, callback: UploadCallback): UploadResult {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        axios(this.buildHttpRequest({
            url: '/api/resources' + targetPath,
            method: 'POST',
            params: {
                "override": false
            },
            onUploadProgress(event) {
                callback.onProgress(event.progress || 0)
            },
            data: file
        })).then(() => callback.onSuccess())
            .catch(e => callback.onError(e));
        return {abort: () => source.cancel()};
    }

    async writeText(item: FileListItem, content: string, progress?: (current: number, total: number) => void): Promise<boolean> {
        await this.execute<void>({
            url: '/api/resources' + item.path,
            data: content,
            onDownloadProgress: event_1 => {
                if (progress) {
                    progress(event_1.progress || 0, event_1.total || 1);
                }
            }
        });
        return true;
    }

    search(search: FileSearch, callback: SearchCallback): UploadResult {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        axios<Array<SearchItem>>(this.buildHttpRequest({
            url: '/api/search/',
            method: 'GET',
            params: {
                "query": encodeURIComponent(search.keyword)
            },
        })).then(rsp => {
            const items = rsp.data;
            callback.onProgress(items.map(e => {
                // 每一项渲染
                let parent = '/';
                let name = e.path;
                const lastIndex = e.path.lastIndexOf("/");
                if (lastIndex > -1) {
                    parent = '/' + e.path.substring(0, lastIndex);
                    name = e.path.substring(lastIndex + 1);
                }
                return {
                    path: e.path,
                    size: 0,
                    name: name,
                    extra: parseFileExtra(name),
                    folder: e.dir,
                    cover: '',
                    updateTime: null,
                    parent: parent
                };
            }))
            callback.onSuccess();
        })
            .catch(e => callback.onError(e));
        return {abort: () => source.cancel()};
    }

    getFileDownloadLinks(items: Array<FileListItem>): Promise<Array<FileDownloadLink>> {
        return Promise.resolve(items.map(item => {
            return {
                host: '',
                path: this._buildImagePreviewUrlPath(item.path, "raw")
            };
        }));
    }

}
