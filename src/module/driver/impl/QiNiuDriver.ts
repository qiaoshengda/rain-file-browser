import Driver from "../Driver";
import FileListItem from "@/domain/file/FileListItem";
import {SearchCallback, UploadCallback, UploadResult} from "@/domain/http/HttpUpload";
import FileSearch from "@/domain/file/FileSearch";
import FileDownloadLink from "@/domain/file/FileDownloadLink";

const RegionMap = {
    uc: {
        "z0": 'http://uc.qiniuapi.com',
        "cn-east-2": "http://uc.qiniuapi.com",
        "z1": "http://uc.qiniuapi.com",
        "z2": "http://uc.qiniuapi.com",
        "na0": "http://uc.qiniuapi.com",
        "as0": "http://uc.qiniuapi.com",
        "ap-northeast-1": "http://uc.qiniuapi.com"
    },
}

export default class QiNiuDriver implements Driver {

    init(): Promise<void> {
        return Promise.resolve();
    }

    list(path: string): Promise<FileListItem[]> {
        throw new Error("Method not implemented.");
    }

    mkdir(name: string, path: string): Promise<void> {
        throw new Error("Method not implemented.");
    }

    rename(item: FileListItem, newName: string, path: string): Promise<void> {
        throw new Error("Method not implemented.");
    }

    delete(items: Array<FileListItem>): Promise<void> {
        throw new Error("Method not implemented.");
    }

    copyFile(path: string, destination: string): Promise<void> {
        throw new Error("Method not implemented.");
    }

    moveFile(path: string, destinationPath: string): Promise<void> {
        throw new Error("Method not implemented.");
    }

    readText(item: FileListItem, progress?: ((current: number, total: number) => void) | undefined): Promise<string> {
        throw new Error("Method not implemented.");
    }

    writeText(item: FileListItem, content: string, progress?: ((current: number, total: number) => void) | undefined): Promise<boolean> {
        throw new Error("Method not implemented.");
    }

    read(item: FileListItem, progress?: ((current: number, total: number) => void) | undefined): Promise<ArrayBuffer> {
        throw new Error("Method not implemented.");
    }

    upload(file: File, targetPath: string, callback: UploadCallback): UploadResult {
        throw new Error("Method not implemented.");
    }

    download(source: FileListItem, targetName: string, targetPath: string, progress: (current: number, total: number) => void): Promise<string> {
        throw new Error("Method not implemented.");
    }

    getFileDownloadLink(item: FileListItem): Promise<string> {
        throw new Error("Method not implemented.");
    }

    destroy(): Promise<void> {
        throw new Error("Method not implemented.");
    }

    touch(path: string, name: string): Promise<void> {
        return Promise.resolve(undefined);
    }


    search(search: FileSearch, callback: SearchCallback): UploadResult {
        throw new Error("Method not implemented.");
    }

    getFileDownloadLinks(items: Array<FileListItem>): Promise<Array<FileDownloadLink>> {
        throw new Error("Method not implemented.");
    }

}
