import FileListItem from "@/domain/file/FileListItem";
import {SearchCallback, UploadCallback, UploadResult} from "@/domain/http/HttpUpload";
import FileSearch from "@/domain/file/FileSearch";
import FileDownloadLink from "@/domain/file/FileDownloadLink";

export default interface Driver {

    /**
     * 初始化，有些驱动需要初始化获取token
     */
    init(): Promise<void>;

    /**
     * 查询全部文件列表
     *
     * @param path 文件路径
     */
    list(path: string): Promise<Array<FileListItem>>;

    /**
     * 新建文件夹
     *
     * @param name 文件夹名
     * @param path 文件路径
     */
    mkdir(name: string, path: string): Promise<void>;

    /**
     * 重命名文件
     *
     * @param item 要修改的文件
     * @param newName 新的文件名
     * @param path 文件所在目录
     */
    rename(item: FileListItem, newName: string, path: string): Promise<void>;

    /**
     * 删除文件
     *
     * @param items 文件
     */
    delete(items: Array<FileListItem>): Promise<void>;

    /**
     * 复制文件
     * @param path 文件路径
     * @param destination 目的地
     */
    copyFile(path: string, destination: string): Promise<void>;

    /**
     * 移动文件
     * @param path 文件路径
     * @param destinationPath 目的地路径
     */
    moveFile(path: string, destinationPath: string): Promise<void>;

    /**
     * 创建文件
     * @param path 文件路径
     * @param name 新的文件名
     */
    touch(path: string, name: string): Promise<void>;

    /**
     * 读取文件文本
     * @param item 要修改的文件
     * @param progress 进度回调
     */
    readText(item: FileListItem, progress?: (current: number, total: number) => void): Promise<string>;

    /**
     * 写入文本
     * @param item 要修改的文件
     * @param content 文件内容
     * @param progress 进度回调
     */
    writeText(item: FileListItem, content: string, progress?: (current: number, total: number) => void): Promise<boolean>;

    /**
     * 上传文件
     * @param file 本地文件
     * @param targetPath 网络文件所在绝对路径
     * @param callback 回调
     * @return 终止操作
     */
    upload(file: File, targetPath: string, callback: UploadCallback): UploadResult;

    /**
     * 下载文件
     * @param source 网络文件信息
     * @param targetName 本地文件名
     * @param targetPath 本地文件所在目录
     * @param progress 事件监听
     * @returns 文件实际路径
     */
    download(source: FileListItem, targetName: string, targetPath: string, progress: (current: number, total: number) => void): Promise<string>;

    /**
     * 获取文件下载地址
     *
     * @param item 要修改的文件
     */
    getFileDownloadLink(item: Pick<FileListItem, 'path' | 'extra'>): Promise<string>;

    /**
     * 销毁客户端
     */
    destroy(): Promise<void>;

    /**
     * 搜索
     *
     * @param search 搜索信息
     * @param callback 结果回调
     * @return 操作
     */
    search(search: Partial<FileSearch>, callback: SearchCallback): UploadResult;

    /**
     * 获取文件下载地址
     *
     * @param items 文件项
     * @return 文件下载地址
     */
    getFileDownloadLinks(items: Array<FileListItem>): Promise<Array<FileDownloadLink>>;

}
