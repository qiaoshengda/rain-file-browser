export interface MovieViewItem {

    id: string;

    name: string;

    cover: string;

}
