/**
 * 影视列表项
 */
export interface MovieListItem {

    /**
     * 唯一标识
     */
    id: string;

    /**
     * 名字
     */
    name: string;

    /**
     * 封面
     */
    cover: string;

    /**
     * 总集数，为0不展示
     */
    total: number;

}
