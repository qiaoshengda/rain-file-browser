import {MovieViewItem} from "@/module/movie/domain/MovieViewItem";
import {MovieMediaTypeEnum} from "@/module/movie/enumeration/MovieMediaTypeEnum";
import {MovieListItem} from "@/module/movie/domain/MovieListItem";

export default interface MovieService {

    /**
     * 初始化服务
     */
    init(): Promise<void>;

    /**
     * 查询全部的视图
     */
    views(): Promise<Array<MovieViewItem>>;

    /**
     * 根据类型查询最近观看的媒体
     *
     * @param mediaType 媒体类型
     * @constructor
     */
    resumeByType(mediaType: MovieMediaTypeEnum): Promise<Array<MovieListItem>>;

    /**
     * 根据类型查询最新的媒体
     *
     * @param mediaType 媒体类型
     * @constructor
     */
    latestByType(mediaType: MovieMediaTypeEnum): Promise<Array<MovieListItem>>;

}
