export enum MovieMediaTypeEnum {

    /**
     * 视频
     */
    VIDEO = 1,

    /**
     * 音频
     */
    AUDIO = 2,

    /**
     * 书籍
     */
    BOOK = 3

}

export function getMediaTypeByJellyfin(mediaType: MovieMediaTypeEnum): string {
    switch (mediaType) {
        case MovieMediaTypeEnum.AUDIO:
            return "Audio";
        case MovieMediaTypeEnum.BOOK:
            return "Book";
        case MovieMediaTypeEnum.VIDEO:
            return "Video";
        default:
            const _n: never = mediaType;
            return "";
    }
}
