import MovieService from "@/module/movie/MovieService";
import {MovieMediaTypeEnum} from "@/module/movie/enumeration/MovieMediaTypeEnum";
import {MovieListItem} from "@/module/movie/domain/MovieListItem";
import {MovieViewItem} from "@/module/movie/domain/MovieViewItem";
import {getDefaultMovieAuth, Movie, MovieAuth} from "@/entity/Movie";
import {UserInfo} from "@/module/movie/impl/jellyfin/define/UserInfoByLogin";
import {login, userInfoByToken} from "@/module/movie/impl/jellyfin/api/UserApi";
import {ViewsApi} from "@/module/movie/impl/jellyfin/api/ViewsApi";

export default class JellyfinMovieServiceImpl implements MovieService {

    private token: string = "";
    private readonly auth: MovieAuth;

    private userInfo?: UserInfo;

    constructor(movie: Movie) {
        // 认证信息
        const auth = Object.assign(getDefaultMovieAuth(), movie.nativeAuth[utools.getNativeId()]);
        this.auth = {
            url: auth.url || movie.auth.url,
            useToken: auth.useToken || movie.auth.useToken,
            username: auth.username || movie.auth.username,
            password: auth.password || movie.auth.password,
            token: auth.token || movie.auth.token,
        }
        console.log(this.auth, movie)
    }

    async init(): Promise<void> {
        if (this.auth.useToken) {
            this.userInfo = await userInfoByToken(this.auth.url, this.auth.token);
        } else {
            this.userInfo = (await login(this.auth.url, this.auth.username, this.auth.password)).User;
        }
        return Promise.resolve();
    }

    async views(): Promise<Array<MovieViewItem>> {
        if (!this.userInfo) {
            return Promise.reject("用户信息不存在");
        }
        const views = await ViewsApi(this.auth.url, this.token, this.userInfo.Id);
        console.log(views)
        return Promise.resolve(views.Items.map(e => ({
            id: e.Id,
            name: e.Name,
            cover: e.ImageTags.Primary ? `${this.auth.url}/Items/${e.DisplayPreferencesId}/Images/Primary?fillHeight=187&fillWidth=333&quality=96&tag=${e.ImageTags.Primary}&quot` : ''
        })));
    }

    latestByType(mediaType: MovieMediaTypeEnum): Promise<Array<MovieListItem>> {
        return Promise.resolve([]);
    }

    resumeByType(mediaType: MovieMediaTypeEnum): Promise<Array<MovieListItem>> {
        return Promise.resolve([]);
    }

}
