export interface Views {
    Items: Item[];
    TotalRecordCount: number;
    StartIndex: number;
}

interface Item {
    Name: string;
    ServerId: string;
    Id: string;
    Etag: string;
    DateCreated: string;
    CanDelete: boolean;
    CanDownload: boolean;
    SortName: string;
    ExternalUrls: any[];
    Path: string;
    EnableMediaSourceDisplay: boolean;
    ChannelId?: any;
    Taglines: any[];
    Genres: any[];
    PlayAccess: string;
    RemoteTrailers: any[];
    ProviderIds: ProviderIds;
    IsFolder: boolean;
    ParentId: string;
    Type: string;
    People: any[];
    Studios: any[];
    GenreItems: any[];
    LocalTrailerCount: number;
    UserData: UserData;
    ChildCount: number;
    SpecialFeatureCount: number;
    DisplayPreferencesId: string;
    Tags: any[];
    CollectionType: string;
    ImageTags: ImageTags;
    BackdropImageTags: any[];
    ImageBlurHashes: ImageBlurHashes;
    LocationType: string;
    LockedFields: any[];
    LockData: boolean;
    PrimaryImageAspectRatio?: PrimaryImageAspectRatio;
}

interface PrimaryImageAspectRatio {
    s: number;
    e: number;
    c: number[];
}

interface ImageBlurHashes {
    Primary?: Primary;
}

interface Primary {
    ad34de80f0ca7484613834891172233b: string;
}

interface ImageTags {
    Primary?: string;
}

interface UserData {
    PlaybackPositionTicks: number;
    PlayCount: number;
    IsFavorite: boolean;
    Played: boolean;
    Key: string;
}

interface ProviderIds {
}
