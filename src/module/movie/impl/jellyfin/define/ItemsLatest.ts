export interface ItemsLatest {
    Name: string;
    ServerId: string;
    Id: string;
    Path: string;
    ChannelId?: any;
    RunTimeTicks: number;
    Type: string;
    UserData: UserData;
    ChildCount: number;
    ImageTags: ImageTags;
    BackdropImageTags: any[];
    ImageBlurHashes: ImageTags;
    LocationType: string;
    MediaType: string;
}

interface ImageTags {
}

interface UserData {
    PlaybackPositionTicks: number;
    PlayCount: number;
    IsFavorite: boolean;
    Played: boolean;
    Key: string;
}
