export interface UserInfoByLogin {
    User: UserInfo;
    SessionInfo: SessionInfo;
    AccessToken: string;
    ServerId: string;
}

interface SessionInfo {
    PlayState: PlayState;
    AdditionalUsers: any[];
    Capabilities: Capabilities;
    RemoteEndPoint: string;
    PlayableMediaTypes: string[];
    Id: string;
    UserId: string;
    UserName: string;
    Client: string;
    LastActivityDate: string;
    LastPlaybackCheckIn: string;
    DeviceName: string;
    DeviceId: string;
    ApplicationVersion: string;
    IsActive: boolean;
    SupportsMediaControl: boolean;
    SupportsRemoteControl: boolean;
    HasCustomDeviceName: boolean;
    ServerId: string;
    SupportedCommands: string[];
}

interface Capabilities {
    PlayableMediaTypes: string[];
    SupportedCommands: string[];
    SupportsMediaControl: boolean;
    SupportsContentUploading: boolean;
    SupportsPersistentIdentifier: boolean;
    SupportsSync: boolean;
}

interface PlayState {
    CanSeek: boolean;
    IsPaused: boolean;
    IsMuted: boolean;
    RepeatMode: string;
}

export interface UserInfo {
    Name: string;
    ServerId: string;
    Id: string;
    HasPassword: boolean;
    HasConfiguredPassword: boolean;
    HasConfiguredEasyPassword: boolean;
    EnableAutoLogin: boolean;
    LastLoginDate: string;
    LastActivityDate: string;
    Configuration: Configuration;
    Policy: Policy;
}

interface Policy {
    IsAdministrator: boolean;
    IsHidden: boolean;
    IsDisabled: boolean;
    BlockedTags: any[];
    EnableUserPreferenceAccess: boolean;
    AccessSchedules: any[];
    BlockUnratedItems: any[];
    EnableRemoteControlOfOtherUsers: boolean;
    EnableSharedDeviceControl: boolean;
    EnableRemoteAccess: boolean;
    EnableLiveTvManagement: boolean;
    EnableLiveTvAccess: boolean;
    EnableMediaPlayback: boolean;
    EnableAudioPlaybackTranscoding: boolean;
    EnableVideoPlaybackTranscoding: boolean;
    EnablePlaybackRemuxing: boolean;
    ForceRemoteSourceTranscoding: boolean;
    EnableContentDeletion: boolean;
    EnableContentDeletionFromFolders: any[];
    EnableContentDownloading: boolean;
    EnableSyncTranscoding: boolean;
    EnableMediaConversion: boolean;
    EnabledDevices: any[];
    EnableAllDevices: boolean;
    EnabledChannels: any[];
    EnableAllChannels: boolean;
    EnabledFolders: any[];
    EnableAllFolders: boolean;
    InvalidLoginAttemptCount: number;
    LoginAttemptsBeforeLockout: number;
    MaxActiveSessions: number;
    EnablePublicSharing: boolean;
    BlockedMediaFolders: any[];
    BlockedChannels: any[];
    RemoteClientBitrateLimit: number;
    AuthenticationProviderId: string;
    PasswordResetProviderId: string;
    SyncPlayAccess: string;
}

interface Configuration {
    PlayDefaultAudioTrack: boolean;
    SubtitleLanguagePreference: string;
    DisplayMissingEpisodes: boolean;
    GroupedFolders: any[];
    SubtitleMode: string;
    DisplayCollectionsView: boolean;
    EnableLocalPassword: boolean;
    OrderedViews: any[];
    LatestItemsExcludes: any[];
    MyMediaExcludes: any[];
    HidePlayedInLatest: boolean;
    RememberAudioSelections: boolean;
    RememberSubtitleSelections: boolean;
    EnableNextEpisodeAutoPlay: boolean;
}
