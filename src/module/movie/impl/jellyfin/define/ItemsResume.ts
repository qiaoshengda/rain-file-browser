export interface ItemsResume {
    Items: Item[];
    TotalRecordCount: number;
    StartIndex: number;
}

interface Item {
    Name: string;
    ServerId: string;
    Id: string;
    Container: string;
    PremiereDate: string;
    ChannelId?: any;
    RunTimeTicks: number;
    ProductionYear: number;
    IndexNumber: number;
    ParentIndexNumber: number;
    IsFolder: boolean;
    Type: string;
    ParentBackdropItemId: string;
    ParentBackdropImageTags: string[];
    UserData: UserData;
    SeriesName: string;
    SeriesId: string;
    SeasonId: string;
    PrimaryImageAspectRatio: PlayedPercentage | number;
    SeriesPrimaryImageTag: string;
    SeasonName: string;
    VideoType: string;
    ImageTags: ImageTags;
    BackdropImageTags: any[];
    ImageBlurHashes: ImageBlurHashes;
    LocationType: string;
    MediaType: string;
    CommunityRating?: number;
}

interface ImageBlurHashes {
    Primary: Primary;
    Backdrop: Backdrop;
}

interface Backdrop {
    ff69239a9b65094b231579c10f99a424?: string;
    '350b9535076da1faef931d582d99942f'?: string;
    '100bc2c82f51638aa4565dff6509ef39'?: string;
}

interface Primary {
    '5118dab480521072d6858ade8a573c22'?: string;
    '07e721358b4e8bcc7d5392dcd98e6654'?: string;
    '2022c6d7b329a63deeb645d0ad634d7f'?: string;
    '3bb9a9dc1c7cd3295ea80de521f878cd'?: string;
    '067f510fefac4b7fc48000446e5675e0'?: string;
    '7f7336285e76cd08dd9a4955b68691b4'?: string;
}

interface ImageTags {
    Primary: string;
}

interface UserData {
    PlayedPercentage: PlayedPercentage;
    PlaybackPositionTicks: number;
    PlayCount: number;
    IsFavorite: boolean;
    LastPlayedDate: string;
    Played: boolean;
    Key: string;
}

interface PlayedPercentage {
    s: number;
    e: number;
    c: number[];
}
