import {Views} from "@/module/movie/impl/jellyfin/define/Views";
import axios from "axios";
import Constant from "@/global/Constant";
import {ItemsResume} from "@/module/movie/impl/jellyfin/define/ItemsResume";
import {ItemsLatest} from "@/module/movie/impl/jellyfin/define/ItemsLatest";

export async function ViewsApi(baseUrl: string, token: string, userId: string): Promise<Views>{
    const rsp = await axios<Views>({
        baseURL: baseUrl,
        url: `/Users/${userId}/Views`,
        headers: {
            'X-Emby-Authorization': 'MediaBrowser Client="Jellyfin Web", ' +
                `Device="${Constant.id}", ` +
                `DeviceId="${utools.getNativeId()}", ` +
                `Version="${Constant.version}", ` +
                `Token="${token}"`
        }
    });
    return Promise.resolve(rsp.data);
}

export async function ResumeApi(baseUrl: string, token: string, userId: string, mediaType: string): Promise<ItemsResume>{
    const rsp = await axios<ItemsResume>({
        baseURL: baseUrl,
        url: `/Users/${userId}/Items/Resume`,
        method: 'GET',
        params: {
            Limit: 10,
            Recursive: true,
            Fields: 'PrimaryImageAspectRatio,BasicSyncInfo',
            ImageTypeLimit: 1,
            EnableImageTypes: 'Primary,Backdrop,Thumb',
            EnableTotalRecordCount: false,
            MediaTypes: mediaType,
        },
        headers: {
            'X-Emby-Authorization': 'MediaBrowser Client="Jellyfin Web", ' +
                `Device="${Constant.id}", ` +
                `DeviceId="${utools.getNativeId()}", ` +
                `Version="${Constant.version}", Token="${token}"`
        }
    });
    return Promise.resolve(rsp.data);
}

export async function LatestApi(baseUrl: string, token: string, userId: string, parentId: string): Promise<ItemsLatest>{
    const rsp = await axios<ItemsLatest>({
        baseURL: baseUrl,
        url: `/Users/${userId}/Items/Latest`,
        method: 'GET',
        params: {
            Limit: 10,
            Fields: 'PrimaryImageAspectRatio,BasicSyncInfo,Path',
            ImageTypeLimit: 1,
            EnableImageTypes: 'Primary,Backdrop,Thumb',
            ParentId: parentId
        },
        headers: {
            'X-Emby-Authorization': 'MediaBrowser Client="Jellyfin Web", ' +
                `Device="${Constant.id}", ` +
                `DeviceId="${utools.getNativeId()}", ` +
                `Version="${Constant.version}", Token="${token}"`
        }
    });
    return Promise.resolve(rsp.data);
}
