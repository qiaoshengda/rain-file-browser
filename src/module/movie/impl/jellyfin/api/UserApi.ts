import axios from "axios";
import Constant from "@/global/Constant";
import {UserInfo, UserInfoByLogin} from "@/module/movie/impl/jellyfin/define/UserInfoByLogin";

export async function login(baseUrl: string, username: string, password: string): Promise<UserInfoByLogin> {
    const rsp = await axios<UserInfoByLogin>({
        baseURL: baseUrl,
        url: '/Users/authenticatebyname',
        data: {
            Username: username,
            Pw: password
        },
        method: 'POST',
        headers: {
            'X-Emby-Authorization': 'MediaBrowser Client="Jellyfin Web", ' +
                `Device="${Constant.id}", ` +
                `DeviceId="${utools.getNativeId()}", ` +
                `Version="${Constant.version}"`
        }
    });
    return Promise.resolve(rsp.data);
}

export async function userInfoByToken(baseUrl: string, token: string): Promise<UserInfo> {
    const rsp = await axios<Array<UserInfo>>({
        baseURL: baseUrl,
        url: '/Users',
        method: 'GET',
        headers: {
            'X-Emby-Authorization': 'MediaBrowser Client="Jellyfin Web", ' +
                `Device="${Constant.id}", ` +
                `DeviceId="${utools.getNativeId()}", ` +
                `Version="${Constant.version}", ` +
                `Token="${token}"`
        }
    });
    return Promise.resolve(rsp.data[0]);

}
