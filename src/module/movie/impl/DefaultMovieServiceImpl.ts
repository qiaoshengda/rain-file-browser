import MovieService from "@/module/movie/MovieService";
import {MovieMediaTypeEnum} from "@/module/movie/enumeration/MovieMediaTypeEnum";
import {MovieListItem} from "@/module/movie/domain/MovieListItem";
import {MovieViewItem} from "@/module/movie/domain/MovieViewItem";

export default class DefaultMovieServiceImpl implements MovieService {
    init(): Promise<void> {
        return Promise.resolve(undefined);
    }

    latestByType(mediaType: MovieMediaTypeEnum): Promise<Array<MovieListItem>> {
        return Promise.resolve([]);
    }

    resumeByType(mediaType: MovieMediaTypeEnum): Promise<Array<MovieListItem>> {
        return Promise.resolve([]);
    }

    views(): Promise<Array<MovieViewItem>> {
        return Promise.resolve([]);
    }

}
