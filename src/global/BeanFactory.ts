import {useEventBus} from "@vueuse/core";
import FileListItem from "@/domain/file/FileListItem";
import Statistics from "@/plugins/Statistics";
import FileMoveProps from "@/domain/file/FileMoveProps";
import VideoPlayItem from "@/domain/media/VideoPlayItem";
import MusicPlayItem from "@/domain/media/MusicPlayItem";

export const statistics = new Statistics();
statistics.init();

// ------------------------ 事件 ------------------------
// 文件打开时间
export const useFileOpenEvent = useEventBus<FileListItem>('file-open');
export const useMusicOpenEvent = useEventBus<FileListItem | null>('music-open');
export const useImageOpenEvent = useEventBus<FileListItem>('image-open');
export const useTextOpenEvent = useEventBus<FileListItem>('text-open');
export const useVideoOpenEvent = useEventBus<FileListItem>('video-open');
export const useOfficeOpenEvent = useEventBus<FileListItem>('office-open');
export const useFileMoveEvent = useEventBus<FileMoveProps>('file-move');
// 歌单事件
export const useMusicPlayEvent = useEventBus<MusicPlayItem>('music-play');
export const useMusicPauseEvent = useEventBus<void>('music-pause');
// 通用事件
export const useUploadEvent = useEventBus<string>('upload');
export const useQuickUploadEvent = useEventBus<string>('quick-upload');
// 视频事件
export const useVideoCreateEvent = useEventBus<FileListItem>('video-create');
export const useVideoPlayEvent = useEventBus<VideoPlayItem>('video-play');
export const useVideoItemAddEvent = useEventBus<number>('video-item-add');
// 音乐服务器
