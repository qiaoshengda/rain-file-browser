import {Log, LogItemEnum} from "@/pages/more/update/entity/Data";

export default [
    {
        version: '1.3.5',
        sign: 135,
        time: '2023-09-07',
        items: [{
            label: LogItemEnum.UPDATE,
            content: "优化文件浏览器，增加多选操作"
        }]
    },
    {
        version: '1.3.4',
        sign: 134,
        time: '2023-09-07',
        items: [{
            label: LogItemEnum.ADD,
            content: "新增音乐服务器，可以播放Navidrome中的音乐了"
        }],
        remark: "下个版本可能会删除影视模块，只保留哔哩哔哩模块，之后增加Jellyfin影视管理，直接观看Jellyfin中的视频，刮削实在是太难搞了"
    },
    {
        version: '1.3.3',
        sign: 133,
        time: '2023-09-01',
        items: [{
            label: LogItemEnum.ADD,
            content: "新增备份设置，可以将数据备份到WebDAV中"
        }, {
            label: LogItemEnum.UPDATE,
            content: '首页改版，布局更加清晰明了'
        }],
    },
    {
        version: '1.3.2',
        sign: 132,
        time: '2023-08-28',
        items: [{
            label: LogItemEnum.ADD,
            content: '新增播放协议设置，可以添加协议，之后在存储中预览视频时跳转协议，也可以在同步设置中设置默认预览视频方式为新增的协议'
        }, {
            label: LogItemEnum.UPDATE,
            content: "优化音乐播放器，新增进度显示，并且现在可以拖拽进度条了"
        }],
    },
    {
        version: '1.3.1',
        sign: 131,
        time: '2023-08-18',
        items: [{
            label: LogItemEnum.ADD,
            content: '新增B站视频，B站视频增加自动获取元数据功能，快去管理你收藏的B站视频吧'
        }, {
            label: LogItemEnum.UPDATE,
            content: "对影视模块进行了优化，整体设计更加合理。"
        }, {
            label: LogItemEnum.UPDATE,
            content: '对首页布局就行了修改，功能更加合理。'
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "优化了AList搜索问题，会在获取驱动时，检测是否开启了索引，开启索引采用索引，未开启采用遍历。"
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "优化了驱动获取方式，加快了系统流畅度。"
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "增加了一些格式的支持"
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "修复了图标混乱的问题"
        }],
    },
    {
        version: '1.3.0',
        sign: 130,
        time: '2023-08- 15',
        items: [{
            label: LogItemEnum.ADD,
            content: '新增影视模块，目前功能比较简单'
        }, {
            label: LogItemEnum.ADD,
            content: "新增缓存模块，目前对音乐进行了缓存，在播放音乐时，会对音乐进行缓存，下次播放直接使用缓存，播放更加流畅"
        }, {
            label: LogItemEnum.ADD,
            content: "Alist增加多账号，不同设备可以使用不同账号，已解决内网穿透或异地组网导致的同一设备不同URL问题"
        }, {
            label: LogItemEnum.UPDATE,
            content: '强化音乐模块，新增音乐列表，音乐扫描'
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "目前存储搜索全部改为遍历文件夹，下个版本会对alist做优化"
        }],
        remark: "本次更新，初步实现了音乐模块；影视模块仅做了最简单的集成，仅完成了最基础的播放功能。"
    },
    {
        version: '1.2.4',
        sign: 124,
        time: '2023-08-07',
        items: [{
            label: LogItemEnum.OPTIMIZATION,
            content: '优化文件上传'
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: '优化文件下载，文件下载进度没有同步刷新，下个版本修复'
        }, {
            label: LogItemEnum.ADD,
            content: "增加文件搜索功能"
        }, {
            label: LogItemEnum.ADD,
            content: "适配utools4.0，实现主面板快速进入存储，但需要将utools搜索框模式设置为聚合搜索，并且在插件应用设置中开启：允许推送内容到搜索面板"
        }],
        remark: "文件搜索功能中，alist使用默认搜索功能，需要在alist设置中开启索引才可以使用，File Browser使用自带的搜索，WebDAV使用文件夹遍历搜索，" +
            "下个版本中，会对alist搜索进行优化，如果没有开启索引，会使用文件夹遍历搜索。"
    },
    {
        version: '1.2.3',
        sign: 123,
        time: '2023-08-04',
        items: [{
            label: LogItemEnum.ADD,
            content: '增加模块：歌单'
        }, {
            label: LogItemEnum.ADD,
            content: '视频预览增加文件列表'
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "优化推荐内容"
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "优化帮助中心"
        }],
    },
    {
        version: '1.2.2',
        sign: 122,
        time: '2023-08-02',
        items: [{
            label: LogItemEnum.ADD,
            content: '增加存储：File Browser'
        }, {
            label: LogItemEnum.ADD,
            content: '首页存储可拖动排序，拖动存储图标即可排序'
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "地址栏过长会显示进度条"
        }],
        remark: "如果大家有什么网络存储想要接入，可以在评论区提出。比如Cloudreve，可道云这样的私有化网盘，其他的商业化网盘建议使用alist"
    },
    {
        version: '1.2.1',
        sign: 121,
        time: '2023-07-31',
        items: [{
            label: LogItemEnum.ADD,
            content: '增加音乐播放器'
        }, {
            label: LogItemEnum.ADD,
            content: '增加办公文件预览'
        }, {
            label: LogItemEnum.ADD,
            content: '视频小窗播放、调用PotPlayer播放'
        }, {
            label: LogItemEnum.ADD,
            content: '平铺布局'
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "优化了复制和移动逻辑，使用更加丝滑"
        }]
    },
    {
        version: '1.2.0',
        sign: 120,
        time: '2023-07-27',
        items: [{
            label: LogItemEnum.REPAIR,
            content: '项目完全重构'
        }, {
            label: LogItemEnum.REPAIR,
            content: '视频播放优化，可以直接在线观看'
        }, {
            label: LogItemEnum.ADD,
            content: "新增网格视图，显示图片、视频预览"
        }, {
            label: LogItemEnum.OPTIMIZATION,
            content: "使用系统文件图标"
        }]
    },
    {
        version: '1.1.0',
        sign: 102,
        time: '2023-05-05',
        items: [{
            label: LogItemEnum.ADD,
            content: '新增Gitee存储'
        }, {
            label: LogItemEnum.ADD,
            content: '新增SFTP'
        }, {
            label: LogItemEnum.ADD,
            content: '新增FTP'
        }, {
            label: LogItemEnum.ADD,
            content: '测试新增SMB，暂时不可用'
        }]
    },
    {
        version: '1.0.2',
        sign: 102,
        time: '2023-04-21',
        items: [{
            label: LogItemEnum.ADD,
            content: '新增Alist V3存储'
        }, {
            label: LogItemEnum.ADD,
            content: '增加全局超时设置'
        }, {
            label: LogItemEnum.REPAIR,
            content: '任务面板修复'
        }]
    },
    {
        version: '1.0.1',
        sign: 101,
        time: '2023-04-20',
        items: [{
            label: LogItemEnum.ADD,
            content: '支持复制外链'
        }, {
            label: LogItemEnum.ADD,
            content: '增加部分文件图标'
        }, {
            label: LogItemEnum.ADD,
            content: '支持文件排序'
        }, {
            label: LogItemEnum.ADD,
            content: '支持表格布局'
        }, {
            label: LogItemEnum.ADD,
            content: '增加批量复制、移动、删除'
        }, {
            label: LogItemEnum.ADD,
            content: '删除存储'
        }, {
            label: LogItemEnum.REPAIR,
            content: '修复任务面板bug'
        }]
    },
    {
        version: '1.0.0',
        sign: 100,
        time: '2023-04-19',
        items: [{
            label: LogItemEnum.ADD,
            content: '第一个版本，全新起航'
        }, {
            label: LogItemEnum.ADD,
            content: '支持WebDav'
        }, {
            label: LogItemEnum.ADD,
            content: '支持文件简单操作'
        }]
    }
] as Log[]
