import UpdateLog from "./UpdateLog";

export default {
    uid: 'zjrzdoei',
    id: 'rain-file-browser',
    name: '听雨文件浏览器',
    version: UpdateLog[0].version,
    description: "两情若是久长时，又岂在朝朝暮暮。",
    repository: 'https://gitee.com/qiaoshengda/rain-file-browser',
    goodsId: "",
    statistics: "http://project-esion.nat300.top",
    homepage: "https://flowus.cn/esion/share/ed96a74f-0519-4c69-ae11-3d7c1a263a7b",
    musicExtra: ['mp3', 'flac', 'ogg', 'wav', 'aac'],
    videoExtra: ['mp4', 'mkv', 'flv', 'm3u8'],
    officeExtra: ['doc', 'docx', 'xls', 'xlsx', 'pdf']
}
