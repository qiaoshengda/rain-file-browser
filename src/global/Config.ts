import StorageTypeEnum from "@/enumeration/StorageTypeEnum";

export default {
    // 驱动颜色
    driver: {
        color(type?: StorageTypeEnum): string {
            switch (type) {
                case StorageTypeEnum.WEBDAV:
                    return 'blue';
                case StorageTypeEnum.ALIST_V3:
                    return 'cyan';
                case StorageTypeEnum.GITEE:
                    return 'red';
                case StorageTypeEnum.FILE_BROWSER:
                    return '#029BF8';
                case StorageTypeEnum.LOCALHOST:
                    return '#FCE695';
                default:
                    return '';
            }
        },
        name(type?: StorageTypeEnum): string {
            switch (type) {
                case StorageTypeEnum.WEBDAV:
                    return 'WebDAV';
                case StorageTypeEnum.ALIST_V3:
                    return 'Alist V3';
                case StorageTypeEnum.GITEE:
                    return 'Gitee';
                case StorageTypeEnum.FILE_BROWSER:
                    return 'File Browser';
                case StorageTypeEnum.LOCALHOST:
                    return '本地文件夹';
                default:
                    return '';
            }
        }
    }
}
