import {defineStore} from "pinia";
import LocalSetting from "@/entity/setting/LocalSetting";
import SettingLayoutEnum from "@/enumeration/SettingLayoutEnum";
import SettingOrderEnum from "@/enumeration/SettingOrderEnum";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {toRaw} from "vue";
import {pathJoin} from "@/utils/FieldUtil";

const downloadPath = utools.getPath('downloads');
const cachePath = utools.getPath('userData');

export function getDefaultLocalSetting(): LocalSetting {
    return {
        downloadPath: downloadPath,
        cachePath: pathJoin(cachePath, 'rain-file-browser'),
        layout: SettingLayoutEnum.GRID,
        order: SettingOrderEnum.DEFAULT,
        maxDownloadCount: 1
    }
}

export const useLocalSettingStore = defineStore('local-setting', {
    state: () => ({
        instance: getDefaultLocalSetting(),
        rev: undefined as string | undefined
    }),
    getters: {
        downloadPath: (state): string => state.instance.downloadPath || downloadPath,
        cachePath: (state): string => state.instance.cachePath || pathJoin(cachePath, 'rain-file-browser'),
        layout: (state): SettingLayoutEnum => state.instance.layout || SettingLayoutEnum.INFO,
        order: (state): SettingOrderEnum => state.instance.order || SettingOrderEnum.DEFAULT,
        maxDownloadCount: (state): number => Math.max(state.instance.maxDownloadCount || 1, 1),
    },
    actions: {
        async init() {
            const res = await utools.db.promises.get(LocalNameEnum.LOCAL_SETTING + utools.getNativeId());
            if (res) {
                this.instance = res.value;
                this.rev = res._rev;
            }
        },
        async save(record: LocalSetting) {
            this.instance = record;
            await this._sync();
        },
        async setLayout(layout: SettingLayoutEnum) {
            this.instance.layout = layout;
            await this._sync();
        },
        async setOrder(order: SettingOrderEnum) {
            this.instance.order = order;
            await this._sync();
        },
        async _sync() {
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.LOCAL_SETTING + utools.getNativeId(),
                _rev: this.rev,
                value: toRaw(this.instance)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        }
    }
})
