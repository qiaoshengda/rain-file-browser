import {defineStore} from "pinia";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {WebStorage} from "@/entity/WebStorage";
import {toRaw} from "vue";
import {map} from "@/utils/ArrayUtil";
import StorageTypeEnum from "@/enumeration/StorageTypeEnum";
import Driver from "@/module/driver/Driver";
import {renderDriver} from "@/store/components/BrowserStore";
import {listByAsync} from "@/utils/utools/DbStorageUtil";


export function getCurrentStoragePrefix(type: StorageTypeEnum, id?: number): string {
    return LocalNameEnum.STORAGE + '/' + id + '/' + type + '/'
}

let lock = false;
let todo = false;

// 驱动：存储ID => 驱动
const driverMap = new Map<number, Driver>();

export const useStorageStore = defineStore('storage', {
    state: () => ({
        storages: new Array<WebStorage>(),
        rev: undefined as string | undefined
    }),
    getters: {
        storageMap: (state): Map<number, WebStorage> => map(state.storages, 'id')
    },
    actions: {
        async init() {
            const {list, rev} = await listByAsync<WebStorage>(LocalNameEnum.STORAGE);
            this.storages = list;
            this.rev = rev;
        },
        async add(record: Omit<WebStorage, 'id' | 'createTime' | 'updateTime'>) {
            const now = new Date();
            const id = now.getTime();
            this.storages.push({
                ...record,
                id,
                createTime: now,
                updateTime: now
            });
            await this._sync();
        },
        async update(id: number, record: Omit<WebStorage, 'id' | 'createTime' | 'updateTime'>) {
            const index = this.storages.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`存储【${id}】不存在`);
            }
            const now = new Date();
            this.storages[index] = {
                ...this.storages[index],
                ...record,
                id,
                updateTime: now
            };
            await this._sync();
            driverMap.delete(id);
        },
        async remove(id: number) {
            const index = this.storages.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`存储【${id}】不存在`);
            }
            this.storages.splice(index, 1);
            await this._sync();
            driverMap.delete(id);
            // TODO: 删除相关视频、相关音乐
        },
        async save(storages: Array<WebStorage>) {
            this.storages = storages;
            await this._sync();
        },
        async _sync() {
            if (lock) {
                todo = true;
                return;
            }
            lock = true;

            const res = await utools.db.promises.put({
                _id: LocalNameEnum.STORAGE,
                _rev: this.rev,
                value: toRaw(this.storages)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
            lock = false;
            if (todo) {
                todo = false;
                await this._sync();
            }
        },
        async getDriver(id: number): Promise<Driver> {
            let driver = driverMap.get(id);
            if (!driver) {
                const index = this.storages.findIndex(e => e.id === id);
                if (index === -1) {
                    return Promise.reject("存储未找到，无法获取驱动");
                }
                driver = await renderDriver(this.storages[index]);
                driverMap.set(id, driver);
            }
            return Promise.resolve(driver);
        }
    }
})
