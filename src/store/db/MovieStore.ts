import {defineStore} from "pinia";
import {Movie} from "@/entity/Movie";
import {listByAsync, saveListByAsync} from "@/utils/utools/DbStorageUtil";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {map} from "@/utils/ArrayUtil";
import {toRaw} from "vue";
import MovieService from "@/module/movie/MovieService";
import MovieTypeEnum from "@/enumeration/MovieTypeEnum";
import JellyfinMovieServiceImpl from "@/module/movie/impl/jellyfin/JellyfinMovieServiceImpl";

let lock = false;
let todo = false;


// 驱动：存储ID => 驱动
const movieServiceMap = new Map<number, MovieService>();

export const useMovieStore = defineStore('movie', {
    state: () => ({
        movies: new Array<Movie>(),
        _rev: undefined as string | undefined
    }),
    getters: {
        movieMap: (state): Map<number, Movie> => map(state.movies, 'id')
    },
    actions: {
        async init() {
            const {list, rev} = await listByAsync<Movie>(LocalNameEnum.MOVIE);
            this.movies = list;
            this._rev = rev;
        },
        async _sync() {
            if (lock) {
                todo = true;
                return;
            }
            lock = true;
            try {
                this._rev = await saveListByAsync(LocalNameEnum.MOVIE, this.movies, this._rev);
            } finally {
                lock = false;
            }
            if (todo) {
                todo = false;
                await this._sync();
            }
        },
        async add(record: Omit<Movie, 'id' | 'createTime' | 'updateTime'>) {
            const now = new Date();
            const id = now.getTime();
            this.movies.push({
                ...record,
                id,
                createTime: now,
                updateTime: now,
                auth: toRaw(record.auth),
                nativeAuth: toRaw(record.nativeAuth)
            });
            await this._sync();
        },
        async update(id: number, record: Omit<Movie, 'id' | 'createTime' | 'updateTime'>) {
            const index = this.movies.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`影视【${id}】不存在`);
            }
            const now = new Date();
            this.movies[index] = {
                ...this.movies[index],
                ...record,
                id,
                updateTime: now,
                auth: toRaw(record.auth),
                nativeAuth: toRaw(record.nativeAuth)
            };
            await this._sync();
            movieServiceMap.delete(id);
        },
        async remove(id: number) {
            const index = this.movies.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`存储【${id}】不存在`);
            }
            this.movies.splice(index, 1);
            await this._sync();
            movieServiceMap.delete(id);
        },
        async getServiceById(id: number): Promise<MovieService> {
            let movieService = movieServiceMap.get(id);
            if (movieService) {
                return movieService;
            }
            const movie = this.movieMap.get(id);
            if (!movie) {
                return Promise.reject("影视未找到，请重试");
            }
            if (movie.type === MovieTypeEnum.JELLYFIN) {
                movieService = new JellyfinMovieServiceImpl(movie);
            } else {
                return Promise.reject("影视类型未知，请设置影视类型");
            }
            await movieService.init();
            movieServiceMap.set(id, movieService);
            return movieService;
        }
    }
})
