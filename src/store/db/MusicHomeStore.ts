import {defineStore} from "pinia";
import {MusicItem} from "@/entity/MusicList";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {toRaw} from "vue";
import FileListItem from "@/domain/file/FileListItem";
import {parseFileName} from "@/utils/FieldUtil";

let init = false;

export const useMusicHomeStore = defineStore('music-home', {
    state: () => ({
        items: new Array<MusicItem>(),
        rev: undefined as string | undefined
    }),
    actions: {
        async init() {
            if (init) {
                return;
            }
            const res = await utools.db.promises.get(LocalNameEnum.MUSIC_HOME);
            if (res) {
                this.items = res.value;
                this.rev = res._rev;
            }
            init = true;
        },
        async add(item: FileListItem, storageId: number) {
            await this.init();
            // 判断此歌曲是否已经存在
            const index = this.items.findIndex(e => e.path === item.path && e.storageId === storageId);
            if (index > -1) {
                return Promise.reject("当前歌曲已经在歌曲列表");
            }
            const now = new Date();
            const id = now.getTime();
            this.items.push({
                id,
                createTime: now,
                updateTime: now,
                path: item.path,
                extra: item.extra,
                name: parseFileName(item.name),
                artist: '',
                cover: '',
                lyric: '',
                storageId
            });
            await this._sync();
        },
        async update(index: number, item: Pick<MusicItem, 'name' | 'cover' | 'lyric' | 'artist'>) {
            await this.init();
            if (index >= this.items.length) {
                return Promise.reject("索引越界，请刷新后重试");
            }
            this.items[index] = {
                ...this.items[index],
                ...item,
                updateTime: new Date(),
                id: this.items[index].id
            };
            await this._sync();
        },
        async remove(id: number) {
            await this.init();
            const index = this.items.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`歌曲【${id}】不存在`);
            }
            this.items.splice(index, 1);
            await this._sync();
        },
        async _sync() {
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.MUSIC_HOME,
                _rev: this.rev,
                value: toRaw(this.items)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        },
        async addAll(items: Array<MusicItem>): Promise<number> {
            let count = 0;
            for (let item of items) {
                const index = this.items.findIndex(e =>
                    e.path === item.path && e.storageId === item.storageId);
                if (index > -1) {
                    continue;
                }
                count += 1;
                this.items.push(item);
            }
            await this._sync();
            return Promise.resolve(count);
        }
    }
})
