import {defineStore} from "pinia";
import {MusicItem, MusicListIndex} from "@/entity/MusicList";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {toRaw} from "vue";
import {map} from "@/utils/ArrayUtil";
import {parseFileName, sleep} from "@/utils/FieldUtil";
import {useBrowserStore} from "@/store/components/BrowserStore";
import FileListItem from "@/domain/file/FileListItem";
import MessageUtil from "@/utils/MessageUtil";

let init = false;

export const useMusicListStore = defineStore('music-list', {
    state: () => ({
        items: new Array<MusicListIndex>(),
        rev: undefined as string | undefined
    }),
    getters: {
        musicMap: state => map(state.items, 'id')
    },
    actions: {
        async init() {
            if (init) {
                return;
            }
            init = true;
            const res = await utools.db.promises.get(LocalNameEnum.MUSIC_LIST);
            if (res) {
                this.items = res.value;
                this.rev = res._rev;
            }
        },
        async add(record: Omit<MusicListIndex, 'id' | 'createTime' | 'updateTime'>) {
            const now = new Date();
            const id = now.getTime();
            this.items.push({
                ...record,
                id,
                createTime: now,
                updateTime: now
            });
            await this._sync();
        },
        async update(id: number, record: Omit<MusicListIndex, 'id' | 'createTime' | 'updateTime'>) {
            const index = this.items.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`歌单【${id}】不存在`);
            }
            const now = new Date();
            this.items[index] = {
                ...this.items[index],
                ...record,
                id,
                updateTime: now
            };
            await this._sync();
        },
        async remove(id: number) {
            const index = this.items.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`歌单【${id}】不存在`);
            }
            this.items.splice(index, 1);
            await this._sync();
            // 删除每一项
            const res = await utools.db.promises.remove(LocalNameEnum.MUSIC_LIST_ITEM + id);
            if (res.error) {
                return Promise.reject(res.message);
            }
        },
        async _sync() {
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.MUSIC_LIST,
                _rev: this.rev,
                value: toRaw(this.items)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        },
        // --------------------------- 音乐项 ---------------------------
        async append(item: MusicItem, musicIds: Array<number>) {
            for (let musicId of musicIds) {
                await sleep(100);
                let items = new Array<MusicItem>();
                let rev = undefined as string | undefined;
                const itemsWrap = await utools.db.promises.get(LocalNameEnum.MUSIC_LIST_ITEM + musicId);
                if (itemsWrap) {
                    items = itemsWrap.value;
                    rev = itemsWrap._rev;
                }
                const now = new Date();
                // 判断此歌曲是否已经存在
                const index = items.findIndex(e => e.path === item.path && e.storageId === useBrowserStore().storage.id);
                if (index > -1) {
                    let temp = this.items.find(e => e.id === musicId);
                    MessageUtil.warning(`当前歌曲已经在歌单【${temp ? temp.name : ''}】中`);
                    continue;
                }
                items.push({
                    ...item,
                    id: now.getTime(),
                    createTime: now,
                    updateTime: now
                });
                await utools.db.promises.put({
                    _id: LocalNameEnum.MUSIC_LIST_ITEM + musicId,
                    _rev: rev,
                    value: items
                });
            }
        }
    }
})
