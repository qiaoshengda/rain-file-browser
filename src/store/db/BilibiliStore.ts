import {toRaw} from "vue";
import {defineStore} from "pinia";
import {Bilibili, BilibiliIndex, BilibiliWrap, getDefaultBilibiliInfo} from "@/entity/bilibili";
import { sleep} from "@/utils/FieldUtil";
import {cacheVideoCover} from "@/utils/CahceUtil";
import MessageUtil from "@/utils/MessageUtil";
import CacheModuleEnum from "@/enumeration/CacheModuleEnum";
import LocalNameEnum from "@/enumeration/LocalNameEnum";


let init = false;

export const useBilibiliStore = defineStore('bilibili', {
    state: () => ({
        bilibiliItems: new Array<BilibiliIndex>(),
        rev: undefined as string | undefined
    }),
    actions: {
        async init() {
            if (init) {
                return Promise.resolve();
            }
            init = true;
            const res = await utools.db.promises.get(LocalNameEnum.BILIBILI);
            if (res) {
                this.bilibiliItems = res.value;
                this.rev = res._rev;
            }
            // 处理是否存在本地文件
            this.bilibiliItems.forEach(bilibili => cacheVideoCover(bilibili, CacheModuleEnum.BILIBILI_COVER));
        },
        async _sync() {
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.BILIBILI,
                _rev: this.rev,
                value: toRaw(this.bilibiliItems)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        },
        async add(record: Omit<BilibiliIndex, 'id' | 'createTime' | 'updateTime'>): Promise<number> {
            if (record.name.trim() === '') {
                return Promise.reject("视频名称不能为空");
            }
            if (record.storageId === 0) {
                return Promise.reject("一定要选择一个存储");
            }
            // 判断是否重复
            const index = this.bilibiliItems.findIndex(e =>
                e.storageId === record.storageId && e.path === record.path);
            if (index > -1) {
                return Promise.reject("视频【" + record.name + "】已存在，不需要添加");
            }
            const now = new Date();
            const bilibili = {
                ...record,
                id: now.getTime(),
                createTime: now,
                updateTime: now,
                localCover: false
            };
            this.bilibiliItems.push(bilibili);
            await this._sync();
            // 立即尝试缓存视频，缓存成功后刷新
            cacheVideoCover(bilibili, CacheModuleEnum.BILIBILI_COVER).then(res => res ? this._sync() : null);
            return Promise.resolve(now.getTime());
        },
        async remove(id: number) {
            const index = this.bilibiliItems.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject("视频不存在");
            }
            this.bilibiliItems.splice(index, 1);
            await this._sync();
            // 删除详细
            await utools.db.promises.remove(LocalNameEnum.BILIBILI_INFO + id);
        },
        async pushAll(bilibiliItems: Array<Bilibili>) {
            for (let bilibili of bilibiliItems) {
                try {
                    await this.push(bilibili);
                    await sleep(100);
                } catch (e) {
                    MessageUtil.warning(`B站视频【${bilibili.index.name}】新增失败`, e)
                }
            }
        },
        async push(bilibili: Bilibili) {
            // 根据当前路径获取
            // 新增索引
            const id = await this.add(bilibili.index);
            // 新增详情
            const infoRes = await utools.db.promises.put({
                _id: LocalNameEnum.BILIBILI_INFO + id,
                value: bilibili.info
            });
            if (infoRes.error) {
                MessageUtil.warning("新增视频详细未成功", infoRes.message);
            }
            return Promise.resolve();
        },
        async getBilibili(id: number): Promise<Bilibili> {
            const index = this.bilibiliItems.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject("视频不存在，请刷新后重试");
            }
            const idx = this.bilibiliItems[index];
            let info = getDefaultBilibiliInfo();
            const infoWrap = await utools.db.promises.get(LocalNameEnum.BILIBILI_INFO + id);
            if (infoWrap) {
                info = Object.assign(info, infoWrap.value);
            }
            return Promise.resolve({
                info: info,
                index: idx
            });
        },
        async updateAllById(id: number, bilibili: BilibiliWrap) {
            const index = this.bilibiliItems.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject("视频不存在");
            }
            this.bilibiliItems[index] = {
                ...this.bilibiliItems[index],
                ...bilibili.index,
                updateTime: new Date(),
                id: id
            };
            await this._sync();
            // 更新详情
            let info = getDefaultBilibiliInfo();
            const infoWrap = await utools.db.promises.get(LocalNameEnum.BILIBILI_INFO + id);
            let rev: string | undefined = undefined;
            if (infoWrap) {
                info = Object.assign(info, infoWrap.value, bilibili.info);
                rev = infoWrap._rev;
            }
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.BILIBILI_INFO + id,
                _rev: rev,
                value: info
            });
            if (res.error) {
                return Promise.reject("修改视频信息异常，" + res.message);
            }
        },
        async update(id: number, record: Pick<BilibiliIndex, 'name' | 'cover' | 'bid'>) {
            const index = this.bilibiliItems.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`视频【${id}】不存在，请刷新后重试`);
            }
            this.bilibiliItems[index] = {
                ...this.bilibiliItems[index],
                ...record,
                updateTime: new Date(),
                id: id
            };
            await this._sync();
            // 立即尝试缓存视频，缓存成功后刷新
            cacheVideoCover(this.bilibiliItems[index], CacheModuleEnum.BILIBILI_COVER).then(res => res ? this._sync() : null);
        },
    }
})
