import {defineStore} from "pinia";
import SyncSetting from "@/entity/setting/SyncSetting";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {toRaw} from "vue";

export function getDefaultSyncSetting(): SyncSetting {
    return {
        textExtra: ['json', 'txt', 'java', 'js', 'ts'],
        imageExtra: ['jpeg', 'png', 'webp', 'jpg'],
        timeout: 5000,
        videoDefaultPlay: 0
    }
}

export const useSyncSettingStore = defineStore(LocalNameEnum.SYNC_SETTING, {
    state: () => ({
        instance: getDefaultSyncSetting(),
        rev: undefined as string | undefined
    }),
    getters: {
        textExtra: (state): Array<string> => state.instance.textExtra || ['json', 'txt', 'java', 'js', 'ts'],
        imageExtra: (state): Array<string> => state.instance.imageExtra || ['jpeg', 'png', 'webp', 'jpg'],
        timeout: (state): number => state.instance.timeout || 5000,
        videoDefaultPlay: state => state.instance.videoDefaultPlay,
    },
    actions: {
        async init() {
            const res = await utools.db.promises.get(LocalNameEnum.SYNC_SETTING);
            if (res) {
                this.instance = Object.assign(this.instance, res.value);
                this.rev = res._rev;
            }
        },
        async save(setting: SyncSetting) {
            this.instance = setting;
            await this._sync();
        },
        async _sync() {
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.SYNC_SETTING,
                _rev: this.rev,
                value: toRaw(this.instance)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        }
    }
})
