import VideoPlayProtocol from "@/entity/setting/VideoPlayProtocol";
import {defineStore} from "pinia";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {toRaw} from "vue";
import {map} from "@/utils/ArrayUtil";

export const useVideoPlayProtocolStore = defineStore(LocalNameEnum.VIDEO_PLAY_PROTOCOL_SETTING, {
    state: () => ({
        videoPlayProtocols: new Array<VideoPlayProtocol>(),
        rev: undefined as string | undefined
    }),
    getters: {
        videoPlayProtocolMap: state => map(state.videoPlayProtocols, 'id')
    },
    actions: {
        async init() {
            const res = await utools.db.promises.get(LocalNameEnum.VIDEO_PLAY_PROTOCOL_SETTING);
            if (res) {
                this.videoPlayProtocols = res.value;
                this.rev = res._rev;
            }
        },
        async _sync() {
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.VIDEO_PLAY_PROTOCOL_SETTING,
                _rev: this.rev,
                value: toRaw(this.videoPlayProtocols)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        },
        async add(item: Omit<VideoPlayProtocol, 'id'>) {
            if (item.name.trim() === '') {
                return Promise.reject("协议名不能为空");
            }
            if (item.template.trim() === '') {
                return Promise.reject("协议模板不能为空");
            }
            if (item.template.indexOf("{url}") === -1) {
                return Promise.reject("协议模板需要有占位符");
            }
            this.videoPlayProtocols.push({
                ...item,
                id: new Date().getTime()
            });
            await this._sync();
        },
        async update(item: VideoPlayProtocol) {
            if (item.name.trim() === '') {
                return Promise.reject("协议名不能为空");
            }
            if (item.template.trim() === '') {
                return Promise.reject("协议模板不能为空");
            }
            if (item.template.indexOf("{url}") === -1) {
                return Promise.reject("协议模板需要有占位符");
            }
            const index = this.videoPlayProtocols.findIndex(e => e.id === item.id);
            if (index === -1) {
                return Promise.reject("视频协议未找到，无法修改");
            }
            this.videoPlayProtocols[index] = item;
            await this._sync();
        },
        async remove(id: number) {
            const index = this.videoPlayProtocols.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject("视频协议未找到，无法删除");
            }
            this.videoPlayProtocols.splice(index, 1);
            await this._sync();
        }
    }
})
