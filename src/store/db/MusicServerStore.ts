import {defineStore} from "pinia";
import {listByAsync, saveListByAsync} from "@/utils/utools/DbStorageUtil";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {map} from "@/utils/ArrayUtil";
import {toRaw} from "vue";
import {MusicServer} from "@/entity/music-server";
import MusicServerService from "@/module/music-server/MusicServerService";
import MusicServerTypeEnum from "@/enumeration/MusicServerTypeEnum";
import NavidromeMusicServerServiceImpl from "@/module/music-server/impl/NavidromeMusicServerServiceImpl";

let lock = false;
let todo = false;


// 驱动：存储ID => 驱动
const musicServerServiceMap = new Map<number, MusicServerService>();

export const useMusicServerStore = defineStore('music-server', {
    state: () => ({
        musicServers: new Array<MusicServer>(),
        _rev: undefined as string | undefined
    }),
    getters: {
        musicServerMap: (state): Map<number, MusicServer> => map(state.musicServers, 'id')
    },
    actions: {
        async init() {
            const {list, rev} = await listByAsync<MusicServer>(LocalNameEnum.MUSIC_SERVER);
            this.musicServers = list;
            this._rev = rev;
        },
        async _sync() {
            if (lock) {
                todo = true;
                return;
            }
            lock = true;
            try {
                this._rev = await saveListByAsync(LocalNameEnum.MUSIC_SERVER, this.musicServers, this._rev);
            } finally {
                lock = false;
            }
            if (todo) {
                todo = false;
                await this._sync();
            }
        },
        async add(record: Omit<MusicServer, 'id' | 'createTime' | 'updateTime'>) {
            const now = new Date();
            const id = now.getTime();
            this.musicServers.push({
                ...record,
                id,
                createTime: now,
                updateTime: now,
                auth: toRaw(record.auth),
                nativeAuth: toRaw(record.nativeAuth)
            });
            await this._sync();
        },
        async update(id: number, record: Omit<MusicServer, 'id' | 'createTime' | 'updateTime'>) {
            const index = this.musicServers.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`影视【${id}】不存在`);
            }
            const now = new Date();
            this.musicServers[index] = {
                ...this.musicServers[index],
                ...record,
                id,
                updateTime: now,
                auth: toRaw(record.auth),
                nativeAuth: toRaw(record.nativeAuth)
            };
            await this._sync();
            musicServerServiceMap.delete(id);
        },
        async remove(id: number) {
            const index = this.musicServers.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`存储【${id}】不存在`);
            }
            this.musicServers.splice(index, 1);
            await this._sync();
            musicServerServiceMap.delete(id);
        },
        async getServiceById(id: number): Promise<MusicServerService> {
            let movieService = musicServerServiceMap.get(id);
            if (movieService) {
                return movieService;
            }
            const musicServer = this.musicServerMap.get(id);
            if (!musicServer) {
                return Promise.reject("影视未找到，请重试");
            }
            if (musicServer.type === MusicServerTypeEnum.NAVIDROME) {
                movieService = new NavidromeMusicServerServiceImpl(musicServer);
            } else {
                return Promise.reject("影视类型未知，请设置影视类型");
            }
            await movieService.init();
            musicServerServiceMap.set(id, movieService);
            return movieService;
        }
    }
})
