import {toRaw} from "vue";
import {defineStore} from "pinia";
// 实体类
import {VideoIndex} from "@/entity/Video/VideoIndex";
import {Video} from "@/entity/Video";
// 枚举
import CacheModuleEnum from "@/enumeration/CacheModuleEnum";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
// 工具类
import {map} from "@/utils/ArrayUtil";
import MessageUtil from "@/utils/MessageUtil";
import {sleep} from "@/utils/FieldUtil";
import {cacheVideoCover} from "@/utils/CahceUtil";

let init = false;

export const useVideoStore = defineStore('video', {
    state: () => ({
        videos: new Array<VideoIndex>(),
        rev: undefined as undefined | string
    }),
    getters: {
        videoMap: state => map(state.videos, 'id')
    },
    actions: {
        async init() {
            if (init) {
                return;
            }
            init = true;
            const res = await utools.db.promises.get(LocalNameEnum.VIDEO);
            if (res) {
                this.videos = res.value;
                this.rev = res._rev;
            }
            // 处理是否存在本地文件
            this.videos.forEach(video => cacheVideoCover(video, CacheModuleEnum.VIDEO_COVER));
        },
        async _sync() {
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.VIDEO,
                _rev: this.rev,
                value: toRaw(this.videos)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        },
        async remove(id: number) {
            // 删除索引
            const index = this.videos.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject("视频未找到，请刷新后重试！");
            }
            this.videos.splice(index, 1);
            await this._sync();
            // 删除详细
            await utools.db.promises.remove(LocalNameEnum.VIDEO_INFO + id);
            // 删除剧集
            await utools.db.promises.remove(LocalNameEnum.VIDEO_SESSION + id);
            // 删除附件
            await utools.db.promises.remove(LocalNameEnum.VIDEO_ATTACHMENT + id);
            // 删除记录
            await utools.db.promises.remove(LocalNameEnum.VIDEO_RECORD + id);
        },
        async update(id: number, record: Pick<VideoIndex, 'name' | 'cover' | 'type'>): Promise<VideoIndex> {
            const index = this.videos.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject(`视频【${id}】不存在，请刷新后重试`);
            }
            this.videos[index] = {
                ...this.videos[index],
                ...record,
                updateTime: new Date(),
                id: id
            };
            await this._sync();
            // 立即尝试缓存视频，缓存成功后刷新
            cacheVideoCover(this.videos[index], CacheModuleEnum.VIDEO_COVER).then(res => res ? this._sync() : null);
            return Promise.resolve(this.videos[index]);
        },
        async add(record: Omit<VideoIndex, 'id' | 'createTime' | 'updateTime'>): Promise<number> {
            if (record.name.trim() === '') {
                return Promise.reject("视频名称不能为空");
            }
            if (record.storageId === 0) {
                return Promise.reject("一定要选择一个存储");
            }
            const now = new Date();
            const video = {
                ...record,
                id: now.getTime(),
                createTime: now,
                updateTime: now,
                localCover: false
            };
            this.videos.push(video);
            await this._sync();
            // 立即尝试缓存视频，缓存成功后刷新
            cacheVideoCover(video, CacheModuleEnum.VIDEO_COVER).then(res => res ? this._sync() : null);
            return Promise.resolve(now.getTime());
        },
        async pushAll(videos: Array<Video>) {
            for (let video of videos) {
                try {
                    await this.push(video);
                    await sleep(100);
                } catch (e) {
                    MessageUtil.warning(`视频【${video.index.name}】新增失败`, e)
                }
            }
        },
        async push(video: Video) {
            // 根据当前路径获取
            // 新增索引
            const id = await this.add(video.index);
            // 新增详情
            const infoRes = await utools.db.promises.put({
                _id: LocalNameEnum.VIDEO_INFO + id,
                value: video.info
            });
            if (infoRes.error) {
                MessageUtil.warning("新增视频详细未成功", infoRes.message);
            }
            // 新增剧集
            const sessionRes = await utools.db.promises.put({
                _id: LocalNameEnum.VIDEO_SESSION + id,
                value: video.sessions
            })
            if (sessionRes.error) {
                MessageUtil.warning("新增视频剧集未成功", sessionRes.message);
            }
            // 新增记录
            const recordRes = await utools.db.promises.put({
                _id: LocalNameEnum.VIDEO_RECORD + id,
                value: video.record
            })
            if (recordRes.error) {
                MessageUtil.warning("新增视频记录未成功", recordRes.message);
            }
            return Promise.resolve();
        }
    }
})
