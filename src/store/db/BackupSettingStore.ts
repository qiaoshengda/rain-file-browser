import {defineStore} from "pinia";
import {BackupSetting, getDefaultBackupSetting} from "@/entity/setting/BackupSetting";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {toRaw} from "vue";
import {getFromOneByAsync} from "@/utils/utools/DbStorageUtil";


export const useBackupSettingStore = defineStore('backup-setting', {
    state: () => ({
        backupSetting: getDefaultBackupSetting(),
        rev: undefined as string | undefined
    }),
    actions: {
        async init() {
            const res = await getFromOneByAsync(LocalNameEnum.KEY_BACKUP, this.backupSetting);
            this.backupSetting = res.record;
            this.rev = res.rev;
        },
        async save(backupSetting: BackupSetting) {
            this.backupSetting = backupSetting;
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.KEY_BACKUP,
                _rev: this.rev,
                value: toRaw(this.backupSetting)
            });
            if (res.error) {
                return Promise.reject(res.message);
            }
            this.rev = res.rev;
        }
    }
})
