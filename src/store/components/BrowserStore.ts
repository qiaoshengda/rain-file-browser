import KeyValue from "@/domain/KeyValue";
import FileListItem from "@/domain/file/FileListItem";
import DefaultDriver from "@/module/driver/impl/DefaultDriver";
import {WebStorage, getDefaultWebStorage} from "@/entity/WebStorage";
import {useLoadingStore} from "@/store/LoadingStore";
import MessageUtil from "@/utils/MessageUtil";
import {defineStore} from "pinia";
import {getCurrentStoragePrefix, useStorageStore} from "@/store/db/StorageStore";
import {useLocalSettingStore} from "@/store/db/LocalSettingStore";
import SettingOrderEnum from "@/enumeration/SettingOrderEnum";
import {statistics, useFileOpenEvent, useMusicOpenEvent} from "@/global/BeanFactory";
import MessageBoxUtil from "@/utils/MessageBoxUtil";
import FileListItemWrap from "@/domain/file/FileListItemWrap";
import {useQuickUploadStore} from "@/store/components/QuickUploadStore";
import Driver from "@/module/driver/Driver";
import StorageTypeEnum from "@/enumeration/StorageTypeEnum";
import WebDavDriver from "@/module/driver/impl/WebDavDriver";
import AlistV3Driver from "@/module/driver/impl/AlistV3Driver";
import GiteeDriver from "@/module/driver/impl/GiteeDriver";
import FileBrowserDriver from "@/module/driver/impl/FileBrowserDriver";
import {getUrlByWebStorage} from "@/utils/FieldUtil";
import LocalhostDriver from "@/module/driver/impl/LocalhostDriver";

export async function renderDriver(storage: WebStorage): Promise<Driver> {
    let driver: Driver;
    // 判断类型
    if (storage.type === StorageTypeEnum.WEBDAV) {
        driver = new WebDavDriver(storage.webdav);
        statistics.access("使用WebDAV");
    } else if (storage.type === StorageTypeEnum.ALIST_V3) {
        driver = new AlistV3Driver(storage.id, storage.alistV3);
        statistics.access("使用Alist", "v3");
    } else if (storage.type === StorageTypeEnum.GITEE) {
        driver = new GiteeDriver(storage.gitee);
        statistics.access("使用Gitee");
    } else if (storage.type === StorageTypeEnum.FILE_BROWSER) {
        driver = new FileBrowserDriver(storage.fileBrowser);
        statistics.access("使用File Browser");
    } else if (storage.type === StorageTypeEnum.LOCALHOST) {
        driver = new LocalhostDriver(storage.localhost);
        statistics.access("使用File Browser");
    } else {
        return Promise.reject("存储类型未知");
    }
    // 初始化驱动
    await driver.init();
    return Promise.resolve(driver);
}

function sort(items: Array<FileListItem>): Array<FileListItem> {
    let order = useLocalSettingStore().order;
    if (order === SettingOrderEnum.NAME_ASC) {
        return items.sort((a, b) => {
            return a.name.localeCompare(b.name, 'zh-CN');
        });
    } else if (order === SettingOrderEnum.NAME_DESC) {
        return items.sort((a, b) => {
            return b.name.localeCompare(a.name, 'zh-CN');
        });
    } else if (order === SettingOrderEnum.UPDATE_TIME_ASC) {
        return items.sort((a, b) => {
            return new Date(a.updateTime || new Date()).getTime() -
                new Date(b.updateTime || new Date()).getTime();
        });
    } else if (order === SettingOrderEnum.UPDATE_TIME_DESC) {
        return items.sort((a, b) => {
            return new Date(b.updateTime || new Date()).getTime() -
                new Date(a.updateTime || new Date()).getTime();
        });
    } else if (order === SettingOrderEnum.TYPE) {
        return items.sort((a, b) => {
            return a.extra.localeCompare(b.extra, 'zh-CN');
        });
    } else {
        return items;
    }
}

interface WebStorageWrap extends WebStorage {

    /**
     * 全部的链接
     */
    urls: Array<string>;

}

export const useBrowserStore = defineStore('browser', {
    state: () => ({
        // 当前路径
        path: '/',
        // 历史路径记录
        paths: new Array<string>(),
        // 当前文件列表
        items: new Array<FileListItemWrap>(),
        // 当前存储数据
        storage: {...getDefaultWebStorage(), urls: []} as WebStorageWrap,
        // 当前驱动
        driver: new DefaultDriver(),
        // 文件浏览器侧边栏
        collapsed: true
    }),
    getters: {
        back: (state) => state.paths.length > 0,
        pathSplit: (state): Array<KeyValue<string, string>> => {
            let items = state.path.split('/');
            let paths = new Array<KeyValue<string, string>>();
            let prefix = '';
            for (let item of items) {
                if (item.trim() === '') {
                    continue;
                }
                prefix = prefix + '/' + item.trim();
                paths.push({
                    key: item,
                    value: prefix
                });
            }
            return paths;
        },
        up: (state): boolean => !!(state.path && state.path !== '/')
    },
    actions: {
        resolve(name: string, path?: string) {
            if (!path) {
                path = this.path;
            }
            if (path.length > 1) {
                return path + '/' + name;
            } else {
                return '/' + name;
            }
        },
        async init(id: number) {
            // 数据初始化
            this.path = '/';
            this.paths = new Array<string>();
            this.items = new Array<FileListItemWrap>();
            // 获取文件
            const storage = useStorageStore().storageMap.get(id);
            if (!storage) {
                return Promise.reject(`存储【${id}】不存在`)
            }
            const urls = new Array<string>();

            urls.push(getUrlByWebStorage(storage))

            // 设备单独设置
            const prefix = getCurrentStoragePrefix(storage.type, storage.id);
            const items = await utools.db.promises.allDocs(prefix);
            items.forEach(item => urls.push(item.value.url));

            this.storage = {
                ...storage,
                urls
            };
            this.driver = await useStorageStore().getDriver(id);
            this.collapsed = true;
        },

        // ===============================================================================================
        // ------------------------------------------- 操作功能 -------------------------------------------
        // ===============================================================================================

        switchCollapsed() {
            this.collapsed = !this.collapsed;
        },
        to(item: Pick<FileListItem, 'path' | 'folder'>) {
            if (item.folder) {
                this.paths.push(this.path);
                this.path = item.path;
                this.render();
            } else {
                useFileOpenEvent.emit(item as FileListItem);
            }
        },
        render() {
            useLoadingStore().start("正在获取目录信息");
            this.driver.list(this.path)
                .then(items => this.items = sort(items)
                    .map(e => ({
                        ...e,
                        quickUpload: useQuickUploadStore().check({
                            path: e.path,
                            storageId: this.storage.id
                        })
                    })))
                .catch(e => {
                    MessageUtil.error("文件列表获取失败", e);
                    this.items = [];
                })
                .finally(() => useLoadingStore().close());
        },
        pre() {
            if (this.paths.length == 0) {
                return;
            }
            this.path = this.paths.pop() || '/';
            this.render();
        },
        home() {
            this.path = '/';
            this.paths = new Array<string>();
            this.render();
        },

        // ===============================================================================================
        // ------------------------------------------- 相关功能 -------------------------------------------
        // ===============================================================================================

        async rm(item: FileListItem) {
            try {
                useLoadingStore().start(`正在删除【${item.name}】`);
                await this.driver.delete([item]);
                MessageUtil.success("删除成功");
                this.render();
            } catch (e) {
                MessageUtil.error("删除失败", e);
            } finally {
                useLoadingStore().close();
            }
        },
        rename(item: FileListItem) {
            MessageBoxUtil.prompt(`请输入新的文件${item.folder ? '夹' : ''}名`,
                `重命名文件${item.folder ? '夹' : ''}`, {
                    confirmButtonText: '重命名',
                    inputValue: item.name
                }).then(value => {
                if (value.trim() === '') {
                    MessageUtil.warning("请输入正确的文件名");
                    return;
                }
                if (/[\/:*?"<>|]/.test(value)) {
                    MessageUtil.warning("文件名不能包含【\\\\/:*?\"<>|】这些非法字符");
                    return;
                }
                useLoadingStore().start("重命名");
                this.driver.rename(item, value, useBrowserStore().path)
                    .then(() => {
                        MessageUtil.success("重命名成功");
                        this.render();
                    }).catch(e => {
                    MessageUtil.error("重命名失败", e);
                }).finally(() => useLoadingStore().close());
            })
        },
        copyLink(item: FileListItem) {
            if (item.folder) {
                MessageUtil.error("文件夹无法获取外链");
                return;
            }
            this.driver.getFileDownloadLink(item)
                .then(value => {
                    utools.copyText(value);
                    MessageUtil.success("已复制到剪切板");
                })
                .catch(e => MessageUtil.error("获取外链失败", e));
        },
        touch(name: string): Promise<void> {
            return this.driver.touch(this.path, name);
        },
        destroy() {
            useMusicOpenEvent.emit(null)
        }
    }
})
