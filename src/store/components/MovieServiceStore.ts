import {defineStore} from "pinia";
import DefaultMovieServiceImpl from "@/module/movie/impl/DefaultMovieServiceImpl";
import {useMovieStore} from "@/store/db/MovieStore";

export const useMovieServiceStore = defineStore('movie-service', {
    state: () => ({
        movieService: new DefaultMovieServiceImpl(),
        isInit: false
    }),
    actions: {
        async init(id: number) {
            this.isInit = false;
            this.movieService = await useMovieStore().getServiceById(id);
            this.isInit = true;
        }
    }
})
