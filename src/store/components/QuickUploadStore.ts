import {defineStore} from "pinia";
import CryptoJS from 'crypto-js'
import Feature from "@/domain/Feature";
import QuickUpload from "@/entity/QuickUpload";
import {set} from "@/utils/ArrayUtil";
import {useBrowserStore} from "@/store/components/BrowserStore";
import Constant from "@/global/Constant";

function buildQuickUploadCode(quickUpload: Pick<QuickUpload, 'path' | 'storageId'>): string {
    let value = CryptoJS.enc.Utf8.parse(JSON.stringify({
        path: quickUpload.path,
        storageId: quickUpload.storageId
    }));
    return CryptoJS.DES.encrypt(value, Constant.uid).toString();
}

function decodeQuickUploadCode(value: string): Pick<QuickUpload, 'storageId' | 'path'> {
    return JSON.parse(CryptoJS.DES.decrypt(value, Constant.uid).toString(CryptoJS.enc.Utf8));
}

function renderFeature(quickUpload: QuickUpload): Feature {
    return {
        code: 'quick-upload:' + buildQuickUploadCode(quickUpload),
        platform: ['darwin', 'win32', 'linux'],
        explain: `将文件快速上传到【${useBrowserStore().storage.name}】的【${quickUpload.path}】`,
        icon: 'public/logo.png',
        cmds: [{
            type: "files",
            // 关键字名称（必须）
            label: quickUpload.keyword || '快速上传',
            // 文件类型 - "file"、"directory" (可选)
            fileType: "file",
            // 名称匹配正则字符串 (可选)
            match: quickUpload.match,
            // 最少文件数 (可选)
            minLength: 1,
        }]
    }
}

export const useQuickUploadStore = defineStore('quick-upload', {
    state: () => ({
        features: new Array<Feature>()
    }),
    getters: {
        codes: (state): Set<string> => set(state.features, 'code'),
        items: (state): Array<QuickUpload> => {
            return state.features.map(e => {
                let codeWrap = e.code.replace("quick-upload:", '');
                const qu = decodeQuickUploadCode(codeWrap);
                let cmd = e.cmds[0] as Record<string, any>;
                return {
                    keyword: cmd.label as string,
                    path: qu.path,
                    storageId: qu.storageId,
                    match: cmd.match
                }
            })
        }
    },
    actions: {
        init() {
            const features = new Array<Feature>();
            const items = utools.getFeatures();
            for (let item of items) {
                if (item.code.startsWith("quick-upload")) {
                    // @ts-ignore
                    items.push(item);
                }
            }
            this.features = features
        },
        check(quickUpload: Pick<QuickUpload, 'path' | 'storageId'>) {
            return this.codes.has(buildQuickUploadCode(quickUpload));
        },
        add(quickUpload: QuickUpload) {
            console.log('开始新增', quickUpload);
            utools.setFeature(renderFeature(quickUpload));
            console.log("新增完成，开始初始化");
            // 以防万一，重新初始化
            this.init();
            console.log("初始化完成，开始刷新页面");
            // 刷新页面
            useBrowserStore().render();
            console.log("结束")
        }
    }
})
