import {defineStore} from "pinia";
import {
    getDefaultVideoIndex,
    getDefaultVideoInfo,
    getDefaultVideoRecord, VideoIndex,
    VideoInfo,
    VideoRecord,
    VideoSession
} from "@/entity/Video";
import {useVideoStore} from "@/store/db/VideoStore";
import LocalNameEnum from "@/enumeration/LocalNameEnum";
import {toRaw} from "vue";
import MessageUtil from "@/utils/MessageUtil";
import Driver from "@/module/driver/Driver";
import {useStorageStore} from "@/store/db/StorageStore";
import MessageBoxUtil from "@/utils/MessageBoxUtil";
import {useVideoPlayEvent} from "@/global/BeanFactory";

let recordLock = false;
let sessionLock = false;
let sessionTodo = false;

export const useVideoPlayStore = defineStore('video-play', {
    state: () => ({
        id: 0,
        index: getDefaultVideoIndex(),
        info: getDefaultVideoInfo(),
        infoRev: undefined as undefined | string,
        sessions: new Array<VideoSession>(),
        sessionRev: undefined as undefined | string,
        record: getDefaultVideoRecord(),
        recordRev: undefined as undefined | string,
        driver: null as Driver | null
    }),
    actions: {
        async init(id: number) {
            this.id = id;
            if (this.driver) {
                this.driver.destroy().finally(() => this.driver = null);
            }
            const video = useVideoStore().videoMap.get(id);
            if (!video) {
                this.id = 0;
                return Promise.reject(`系统异常，视频【${id}】未找到`)
            }
            // 索引信息信息
            this.index = Object.assign(getDefaultVideoIndex(), video);
            // 基础信息
            const infoWrap = await utools.db.promises.get(LocalNameEnum.VIDEO_INFO + id);
            if (infoWrap) {
                this.info = Object.assign(getDefaultVideoInfo(), infoWrap.value);
                this.infoRev = infoWrap._rev;
            } else {
                this.info = getDefaultVideoInfo();
                this.infoRev = undefined;
            }
            // 剧集信息
            const sessionWrap = await utools.db.promises.get(LocalNameEnum.VIDEO_SESSION + id);
            if (sessionWrap) {
                this.sessions = sessionWrap.value;
                this.sessionRev = sessionWrap._rev;
            } else {
                this.sessions = new Array<VideoSession>();
                this.sessionRev = undefined;
            }
            const recordWrap = await utools.db.promises.get(LocalNameEnum.VIDEO_RECORD + id);
            if (recordWrap) {
                this.record = Object.assign(getDefaultVideoRecord(this.sessions), recordWrap.value);
                this.recordRev = recordWrap._rev;
            } else {
                this.record = getDefaultVideoRecord(this.sessions);
                this.recordRev = undefined;
            }
        },
        updateIndex(index: VideoIndex) {
            this.index = Object.assign(this.index, index);
        },
        async updateInfo(info: VideoInfo) {
            this.info = Object.assign(this.info, info);
            const res = await utools.db.promises.put({
                _id: LocalNameEnum.VIDEO_INFO + this.id,
                _rev: this.infoRev,
                value: toRaw(this.info)
            })
            if (res.error) {
                MessageUtil.error("修改失败", res.message);
                return;
            }
            // 从新赋值
            this.infoRev = res.rev;
        },
        async getDriver(): Promise<Driver> {
            if (!this.driver) {
                this.driver = await useStorageStore().getDriver(this.index.storageId);
            }
            return this.driver;
        },
        async addSession(name: string): Promise<number> {
            const id = new Date().getTime();
            this.sessions.push({id, name, items: []});
            await this._syncSession();
            return Promise.resolve(id);
        },
        async removeSession(id: number): Promise<void> {
            const index = this.sessions.findIndex(e => e.id === id);
            if (index === -1) {
                return Promise.reject("记录未找到，请刷新后重试！");
            }
            const session = this.sessions[index];
            if (session.items.length > 0) {
                try {
                    await MessageBoxUtil.confirm(`此记录下存在${session.items.length}个视频，确认删除？`, "删除警告", {
                        confirmButtonText: "删除",
                        cancelButtonText: "取消"
                    });
                } catch (e) {
                    return Promise.reject("取消删除")
                }
            }
            this.sessions.splice(index, 1);
            await this._syncSession();
        },
        async _syncSession() {
            if (sessionLock) {
                sessionTodo = true;
                return Promise.resolve();
            }
            sessionLock = true;
            sessionTodo = false;
            try {
                const res = await utools.db.promises.put({
                    _id: LocalNameEnum.VIDEO_SESSION + this.id,
                    _rev: this.sessionRev,
                    value: toRaw(this.sessions)
                });
                if (res.error) {
                    this.sessions.pop();
                    return Promise.reject(res.message);
                }
                this.sessionRev = res.rev;
            } catch (e) {
                throw e;
            } finally {
                sessionLock = false;
                if (sessionTodo) {
                    await this._syncSession();
                }
            }
        },
        async updateRecord(record: Partial<VideoRecord>) {
            if (recordLock) {
                return;
            }
            recordLock = true;
            this.record = Object.assign(this.record, record);
            try {
                const res = await utools.db.promises.put({
                    _id: LocalNameEnum.VIDEO_RECORD + this.id,
                    _rev: this.recordRev,
                    value: toRaw(this.record)
                });
                if (res.error) {
                    return Promise.reject(res.message);
                }
                this.recordRev = res.rev;
            } catch (e) {
                return Promise.reject(e);
            } finally {
                recordLock = false;
            }
        },
        async addItem(sessionId: number, name: string, path: string) {
            // 寻找季
            let session: null | VideoSession = null;
            for (let item of this.sessions) {
                if (item.id === sessionId) {
                    session = item;
                    break;
                }
            }
            if (!session) {
                return Promise.reject("季【" + sessionId + "】未找到，请刷新后重试");
            }
            session.items.push({
                id: new Date().getTime(),
                name: name,
                path: path
            });
            await this._syncSession();
        },
        async updateItem(sessionIndex: number, itemIndex: number, name: string) {
            const session = this.sessions[sessionIndex];
            session.items[itemIndex] = {
                ...session.items[itemIndex],
                name: name
            };
            await this._syncSession();
        },
        async removeItem(sessionIndex: number, itemIndex: number) {
            const session = this.sessions[sessionIndex];
            session.items.splice(itemIndex, 1);
            await this._syncSession();
        },
        play() {
            for (let session of this.sessions) {
                if (session.id === this.record.sessionId) {
                    useVideoPlayEvent.emit({
                        id: this.record.itemId,
                        items: session.items
                    });
                    return;
                }
            }
            // 播放第一个
            if (this.sessions.length > 0) {
                const items = this.sessions[0].items;
                if (items.length > 0) {
                    useVideoPlayEvent.emit({
                        id: items[0].id,
                        items
                    });
                    return;
                }
            }
            MessageUtil.warning("当前剧集没有可播放的视频")
        }
    }
})
