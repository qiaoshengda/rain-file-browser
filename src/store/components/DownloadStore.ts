import {defineStore} from "pinia";
import FileListItem from "@/domain/file/FileListItem";
import Driver from "@/module/driver/Driver";
import {useLocalSettingStore} from "@/store/db/LocalSettingStore";
import MessageUtil from "@/utils/MessageUtil";

export interface DownloadItem {

    item: FileListItem;

    status: 'normal' | 'success' | 'warning' | 'danger';

    current: number;

    total: number;

    path: string

}

export const useDownloadStore = defineStore('download', {
    state: () => ({
        items: new Array<DownloadItem>()
    }),
    actions: {
        add(file: FileListItem, driver: Driver) {
            const item: DownloadItem = {
                item: file,
                status: 'warning',
                current: 0,
                total: 1,
                path: useLocalSettingStore().downloadPath + '/' + file.name
            };
            MessageUtil.info(`文件【${file.name}】开始下载`);
            driver.download(file, file.name, useLocalSettingStore().downloadPath,
                (current, total) => {
                    item.current = current;
                    item.total = total;
                    item.status = 'normal';
                }).then(path => {
                MessageUtil.success(`文件【${file.name}】下载完成`);
                item.status = 'success';
                item.path = path;
            }).catch(e => {
                MessageUtil.success(`文件【${file.name}】下载失败`, e);
                item.status = 'danger';
            });
            this.items.unshift(item);
        }
    }
})
