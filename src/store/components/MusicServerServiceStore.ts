import {defineStore} from "pinia";
import DefaultMusicServerService from "@/module/music-server/impl/DefaultMusicServerService";
import {useMusicServerStore} from "@/store/db/MusicServerStore";
import {MusicItem} from "@/entity/MusicList";
import {statistics, useMusicPauseEvent} from "@/global/BeanFactory";
import {clone} from "xe-utils";
import MusicPlayItem from "@/domain/media/MusicPlayItem";
import MessageUtil from "@/utils/MessageUtil";

const audio = new Audio();

export const useMusicServerServiceStore = defineStore('music-server-service', {
    state: () => ({
        musicServer: new DefaultMusicServerService(),
        isInit: false,

        // 播放操作
        index: -1,
        items: new Array<MusicItem>(),
        progress: 0,
        total: 1,
        isPlay: false,

        // 显示信息
        cover: '',
        name: 'Music Player',
        artist: 'No Artist'
    }),
    actions: {
        prefix() {
            audio.addEventListener('timeupdate', e => {
                this.progress = Math.floor((e.target as HTMLAudioElement).currentTime);
            });
            audio.addEventListener('canplaythrough', e => {
                this.total = (e.target as HTMLAudioElement).duration
            })
            audio.addEventListener('play', () => this.isPlay = true);
            audio.addEventListener('pause', () => this.isPlay = false);
            audio.addEventListener('ended', () => {
                this.isPlay = false;
                if (this.index < this.items.length - 1) {
                    this.next();
                }
            });
            useMusicPauseEvent.reset();
            useMusicPauseEvent.on(() => {
                if (audio) {
                    if (this.isPlay) {
                        audio.pause();
                    }
                    this.isPlay = false
                }
            });
        },
        async init(id: number) {
            this.isInit = false;
            this.musicServer = await useMusicServerStore().getServiceById(id);
            this.isInit = true;
        },

        start(event: MusicPlayItem) {

            this.items = clone(event.items);
            statistics.access("播放音乐音乐")
            this.execute(event.index);
        },

        execute(i: number) {
            if (!audio) {
                MessageUtil.error("系统异常，音频模块未初始化");
                return;
            }
            this.index = i;
            const item = this.items[this.index];
            if (!item) {
                MessageUtil.warning("系统异常，歌曲不存在");
                return;
            }
            audio.src = item.path;
            this.cover = item.cover;
            this.name = item.name;
            this.artist = item.artist;
            this.isPlay = false;
            this.play()

        },
        play() {
            if (!audio) {
                MessageUtil.error("系统异常，音频模块未初始化");
                return;
            }
            if (this.isPlay) {
                audio.pause();
            } else {
                audio.play();
            }
            this.isPlay = !this.isPlay;
        },
        pre() {
            if (this.index <= 0) {
                MessageUtil.warning("不能再往前了");
                return;
            }
            this.execute(this.index - 1);
        },
        next() {
            if (this.index >= this.items.length - 1) {
                MessageUtil.warning("不能再往后了");
                return;
            }
            this.execute(this.index + 1);
        },
        remove(i: number) {
            this.items.splice(i, 1);
            if (i < this.index) {
                this.index -= 1;
            }
        },
        changeProgress(time: number | number[]) {
            if (!audio) {
                // 没有音乐不处理
                return;
            }
            audio.currentTime = typeof time === 'number' ? time : time[0];
        }
    }
});

// 支持前置
useMusicServerServiceStore().prefix();
