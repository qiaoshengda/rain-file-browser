import {defineStore} from "pinia";
import MessageUtil from "@/utils/MessageUtil";
import {useSyncSettingStore} from "@/store/db/SyncSettingStore";

export const useLoadingStore = defineStore('loading', {
    state: () => ({
        loading: false,
        text: '',
        timeId: null as any
    }),
    actions: {
        start(text?: string) {
            this.loading = true;
            this.text = text || '';
            if (this.timeId) {
                clearTimeout(this.timeId);
            }
            this.timeId = setTimeout(() => {
                this.loading = false;
                this.text = '';
                MessageUtil.error("读取超时！")
                this.timeId = null;
            }, useSyncSettingStore().timeout);
        },
        setText(text: string) {
            this.start(text);
        },
        close() {
            this.loading = false;
            this.text = '';
            if (this.timeId) {
                clearTimeout(this.timeId);
            }
        }
    }
})
