export interface MusicItem {

    id: number;

    createTime: Date | string;

    updateTime: Date | string;

    // ------------------------------ 元数据 ------------------------------

    /**
     * 存储ID
     */
    storageId: number;

    /**
     * 文件路径
     */
    path: string;

    /**
     * 文件拓展名
     */
    extra: string;

    // ------------------------------ 歌曲信息 ------------------------------

    /**
     * 歌曲名称
     */
    name: string;

    /**
     * 艺术家
     */
    artist: string;

    /**
     * 封面（路径或链接）
     */
    cover: string;

    /**
     * 歌词（路径或链接）
     */
    lyric: string;

}
