export interface MusicListIndex {

    id: number;

    createTime: Date | string;

    updateTime: Date | string;

    /**
     * 歌单名称
     */
    name: string;

    /**
     * 歌单描述
     */
    description: string;

    /**
     * 头像颜色
     */
    color: string;

}
