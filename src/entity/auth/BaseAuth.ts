export default interface BaseAuth {

    /**
     * 地址
     */
    url: string;

    /**
     * 用户名
     */
    username: string;

    /**
     * 密码
     */
    password: string;

}
