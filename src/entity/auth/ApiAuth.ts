export default interface ApiAuth {

    /**
     * 地址
     */
    url: string;

    /**
     * token
     */
    token: string;

}
