import StorageTypeEnum from "@/enumeration/StorageTypeEnum";
import BaseAuth from "@/entity/auth/BaseAuth";
import StorageByAlistV3 from "@/entity/WebStorage/info/StorageByAlistV3";
import StorageByQiNiu from "@/entity/WebStorage/info/StorageByQiNiu";
import StorageByGitee from "@/entity/WebStorage/info/StorageByGitee";

export interface WebStorage{

    id: number;

    /**
     * 创建时间
     */
    createTime: Date | number;

    /**
     * 更新时间
     */
    updateTime: Date | number;

    /**
     * 存储的名字
     */
    name: string;

    /**
     * 分组
     */
    group: string;

    /**
     * 描述
     */
    description: string;

    /**
     * 存储类型
     */
    type: StorageTypeEnum;

    /**
     * 排序
     */
    sort: number;

    /**
     * WebDav配置，当类型是webdav时有效
     */
    webdav: BaseAuth;

    /**
     * alist v3
     */
    alistV3: StorageByAlistV3;

    /**
     * gitee
     */
    gitee: StorageByGitee;

    /**
     * 文件浏览器
     */
    fileBrowser: BaseAuth;

    /**
     * 七牛云存储
     */
    qiniu: StorageByQiNiu;

    /**
     * 本地
     */
    localhost: BaseAuth;

}


export function getDefaultWebStorage(): WebStorage {
    const now = new Date();
    return {
        id: 0,
        createTime: now,
        updateTime: now,
        name: '',
        group: '',
        description: '',
        sort: 0,
        type: StorageTypeEnum.WEBDAV,
        webdav: {
            url: '',
            username: '',
            password: ''
        },
        fileBrowser: {
            url: '',
            username: '',
            password: ''
        },
        alistV3: {
            url: '',
            username: '',
            password: '',
            token: ''
        },
        gitee: {
            owner: '',
            repo: '',
            branch: '',
            accessToken: ''
        },
        qiniu: {
            accessKey: '',
            secretKey: '',
            region: '',
            bucket: '',
            host: ''
        },
        localhost: {
            url: '',
            username: '',
            password: ''
        }
    }
}

