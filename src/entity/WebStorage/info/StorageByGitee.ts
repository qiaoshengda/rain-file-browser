export default interface StorageByGitee {

    /**
     * 所属
     */
    owner: string;

    /**
     * 名字
     */
    repo: string;

    /**
     * 分支，如果不输入，则列出全部分支
     */
    branch: string;

    /**
     * 访问token
     */
    accessToken: string;

}