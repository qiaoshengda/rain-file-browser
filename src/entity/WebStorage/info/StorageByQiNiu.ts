export default interface StorageByQiNiu {

    /**
     * 访问key
     */
    accessKey: string;

    /**
     * 密钥
     */
    secretKey: string

    /**
     * 区域
     */
    region: string;

    /**
     * 桶
     */
    bucket: string;

    /**
     * 下载域名
     */
    host: string;

}