import BaseAuth from "@/entity/auth/BaseAuth";

export default interface StorageByAlistV3 extends BaseAuth{


    /**
     * token
     */
    token: string;


}
