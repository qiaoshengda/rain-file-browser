import MovieTypeEnum from "@/enumeration/MovieTypeEnum";

export interface Movie {

    id: number;

    createTime: Date | string;

    updateTime: Date | string;

    /**
     * 名字
     */
    name: string;

    /**
     * 类型
     */
    type:MovieTypeEnum;

    /**
     * 认证
     */
    auth: MovieAuth;

    /**
     * 设备绑定
     */
    nativeAuth: Record<string, MovieAuth>;

}


export interface MovieAuth {

    /**
     * 地址
     */
    url: string;

    /**
     * 用户名
     */
    username: string;

    /**
     * 密码
     */
    password: string;

    /**
     * 是否使用TOKEN
     */
    useToken: boolean;

    /**
     * token
     */
    token: string;

}

export function getDefaultMovie(): Movie {
    return {
        id: 0,
        createTime: '',
        updateTime: '',
        name: '',
        type: MovieTypeEnum.JELLYFIN,
        auth: getDefaultMovieAuth(),
        nativeAuth: {}
    }
}

export function getDefaultMovieAuth(): MovieAuth {
    return {
        url: '',
        username: '',
        password: '',
        useToken: false,
        token: ''
    }
}
