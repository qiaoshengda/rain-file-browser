/**
 * 视频记录
 */
export interface VideoRecord {

    /**
     * 当前播放的季ID
     */
    sessionId: number;

    /**
     * 当前播放的ID
     */
    itemId: number;

    /**
     * 片头秒数
     */
    headerTime: number;

    /**
     * 片尾秒数
     */
    footerTime: number;

}
