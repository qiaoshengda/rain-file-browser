import VideoTypeEnum from "@/enumeration/VideoTypeEnum";

/**
 * 视频索引
 */
export interface VideoIndex {

    id: number;

    createTime: Date | string;

    updateTime: Date | string;

    /**
     * 视频名称
     */
    name: string;

    /**
     * 视频的封面
     */
    cover: string;

    /**
     * 类型
     */
    type: VideoTypeEnum;

    /**
     * 所属存储
     */
    storageId: number;

    /**
     * 本地封面是否存在
     */
    localCover: boolean;

}
