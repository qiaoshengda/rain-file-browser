/**
 * 视频的基本信息
 */
export interface VideoInfo {

    /**
     * 语言
     */
    language: string;

    /**
     * 标签
     */
    tags: Array<string>;

    /**
     * 描述
     */
    description: string;

    /**
     * 导演
     */
    director: Array<string>;

    /**
     * 编剧
     */
    writer: Array<string>;

    /**
     * 主角/主演
     */
    lead: Array<string>;

    /**
     * 上映时间
     */
    createDate: string;

}
