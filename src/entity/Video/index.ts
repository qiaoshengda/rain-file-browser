import {VideoIndex} from "@/entity/Video/VideoIndex";
import {VideoInfo} from "@/entity/Video/VideoInfo";
import {VideoSession} from "@/entity/Video/VideoSession";
import {VideoRecord} from "@/entity/Video/VideoRecord";

export * from './VideoIndex';
export * from './VideoInfo';
export * from './VideoItem';
export * from './VideoSession';
export * from './VideoRecord';
export * from './VideoDefault';

export interface Video {
    index: VideoIndex;
    info: VideoInfo;
    sessions: Array<VideoSession>;
    record: VideoRecord;
}


