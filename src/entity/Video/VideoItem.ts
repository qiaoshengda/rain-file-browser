/**
 * 每一个视频项，对于剧集来说，一季有多个项
 * 对于电影来说，只有一项
 */
export interface VideoItem {

    id: number;

    name: string;

    path: string;

}
