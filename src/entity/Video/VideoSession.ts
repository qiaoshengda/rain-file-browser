import {VideoItem} from "@/entity/Video/VideoItem";

/**
 * <p>剧集的一季，内部存放数组，每一季</p>
 *
 * <p>剧集可以有多季，默认只有一季</p>
 * <p>电影只有一季，一季只有一集</p>
 */
export interface VideoSession {

    id: number;

    /**
     * 季名
     */
    name: string;

    items: Array<VideoItem>;

}
