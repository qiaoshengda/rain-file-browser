import {VideoIndex} from "@/entity/Video/VideoIndex";
import VideoTypeEnum from "@/enumeration/VideoTypeEnum";
import {VideoInfo} from "@/entity/Video/VideoInfo";
import {VideoSession} from "@/entity/Video/VideoSession";
import {VideoRecord} from "@/entity/Video/VideoRecord";

export function getDefaultVideoIndex(): VideoIndex {
    return {
        id: 0,
        createTime: '',
        updateTime: '',
        name: '',
        cover: '',
        type: VideoTypeEnum.MOVIE,
        storageId: 0,
        localCover: false
    };
}

export function getDefaultVideoInfo(): VideoInfo {
    return {
        tags: [],
        director: [],
        lead: [],
        createDate: '',
        description: '',
        language: '',
        writer: []
    };
}

export function getDefaultVideoRecord(sessions?: Array<VideoSession>): VideoRecord {
    let sessionId = 0;
    let itemId = 0;
    if (sessions) {
        if (sessions.length > 0) {
            sessionId = sessions[0].id;
            if (sessions[0].items.length > 0) {
                itemId = sessions[0].items[0].id
            }
        }
    }
    return {sessionId, itemId, headerTime: 0, footerTime: 0}
}
