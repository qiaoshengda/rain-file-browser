export default interface QuickUpload {

    /**
     * 提示关键字
     */
    keyword: string;

    /**
     * 快速上传的路径
     */
    path: string;

    /**
     * 存储ID
     */
    storageId: number;

    /**
     * <p>文件类型后缀</p>
     * <p>哪种文件可以触发此上传，默认全部都可以</p>
     */
    match: string;

}
