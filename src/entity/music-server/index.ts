import MusicServerTypeEnum from "@/enumeration/MusicServerTypeEnum";

export interface MusicServer {

    id: number;

    createTime: Date | string;

    updateTime: Date | string;

    /**
     * 名字
     */
    name: string;

    /**
     * 类型
     */
    type:MusicServerTypeEnum;

    /**
     * 认证
     */
    auth: MusicServerAuth;

    /**
     * 设备绑定
     */
    nativeAuth: Record<string, MusicServerAuth>;

}


export interface MusicServerAuth {

    /**
     * 地址
     */
    url: string;

    /**
     * 用户名
     */
    username: string;

    /**
     * 密码
     */
    password: string;

    /**
     * 是否使用TOKEN
     */
    useToken: boolean;

    /**
     * token
     */
    token: string;

}

export function getDefaultMusicServer(): MusicServer {
    return {
        id: 0,
        createTime: '',
        updateTime: '',
        name: '',
        type: MusicServerTypeEnum.NAVIDROME,
        auth: getDefaultMusicServerAuth(),
        nativeAuth: {}
    }
}

export function getDefaultMusicServerAuth(): MusicServerAuth {
    return {
        url: '',
        username: '',
        password: '',
        useToken: false,
        token: ''
    }
}
