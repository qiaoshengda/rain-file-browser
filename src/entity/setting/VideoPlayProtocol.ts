/**
 * 视频播放协议
 */
export default interface VideoPlayProtocol {

    id: number;

    /**
     * 名字
     */
    name: string;

    /**
     * 模板
     */
    template: string;

}
