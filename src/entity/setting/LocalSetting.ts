import SettingLayoutEnum from "@/enumeration/SettingLayoutEnum";

export default interface LocalSetting {

    /**
     * 下载路径
     */
    downloadPath: string;

    /**
     * 缓存目录
     */
    cachePath: string;

    /**
     * 布局方式
     */
    layout: SettingLayoutEnum;

    /**
     * 排序方式
     */
    order: number;

    /**
     * 最大下载数量
     */
    maxDownloadCount: number;

}
