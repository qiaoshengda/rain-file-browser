export default interface SyncSetting {

    /**
     * 文件拓展名
     */
    textExtra: Array<string>;

    /**
     * 图片拓展名
     */
    imageExtra: Array<string>;

    /**
     * 全局超时时间
     */
    timeout: number;

    /**
     * 视频默认播放方式
     * 0: 内部
     * -1: 小窗
     */
    videoDefaultPlay: number,

}
