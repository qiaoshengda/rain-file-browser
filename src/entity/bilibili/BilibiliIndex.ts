export interface BilibiliIndex {

    id: number;

    createTime: Date | string;

    updateTime: Date | string;

    /**
     * 视频名称
     */
    name: string;

    /**
     * 视频的封面
     */
    cover: string;

    /**
     * 哔哩哔哩的ID
     */
    bid: string;

    /**
     * 本地封面是否存在
     */
    localCover: boolean;

    /**
     * 是否喜欢
     */
    love: boolean;

    /**
     * 所在存储
     */
    storageId: number;

    /**
     * 视频地址
     */
    path: string;

}
