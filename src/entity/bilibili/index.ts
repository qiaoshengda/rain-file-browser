import {BilibiliIndex} from "@/entity/bilibili/BilibiliIndex";
import {BilibiliInfo} from "@/entity/bilibili/BilibiliInfo";

export * from './BilibiliIndex';
export * from './BilibiliInfo';

export interface Bilibili {

    /**
     * 索引信息
     */
    index: BilibiliIndex;

    /**
     * 详情
     */
    info: BilibiliInfo

}

export interface BilibiliWrap {
    index: Pick<BilibiliIndex, 'name' | 'cover' | 'bid'>;
    info: Omit<BilibiliInfo, 'extra'>;
}

export function getDefaultBilibili(): Bilibili {
    return {
        info: getDefaultBilibiliInfo(),
        index: getDefaultBilibiliIndex()
    }
}

export function getDefaultBilibiliIndex(): BilibiliIndex {
    return {
        id: 0,
        createTime: '',
        updateTime: '',
        cover: '',
        love: false,
        name: '',
        bid: '',
        storageId: 0,
        path: '',
        localCover: false
    };
}

export function getDefaultBilibiliInfo(): BilibiliInfo {
    return {
        upSpaceId: '',
        tags: [],
        upNickname: '',
        publishTime: '',
        description: '',
        extra: ''
    }
}
