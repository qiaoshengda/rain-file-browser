export interface BilibiliInfo {

    /**
     * 拓展名
     */
    extra: string;

    /**
     * 标签
     */
    tags: Array<string>;

    /**
     * 描述
     */
    description: string;

    /**
     * UP主昵称
     */
    upNickname: string;

    /**
     * UP主的空间ID
     */
    upSpaceId: string;

    /**
     * 发布时间
     */
    publishTime: string;

}
