enum LocalNameEnum {

    /**
     * 存储
     */
    STORAGE = '/db/storage',

    /**
     * 影视
     */
    MOVIE = '/db/movie',

    /**
     * 音乐
     */
    MUSIC_SERVER = '/db/music-server',

    /**
     * 歌单
     */
    MUSIC_LIST = '/db/music/list',

    /**
     * 歌单列表项
     */
    MUSIC_LIST_ITEM = '/db/music/list/',

    /**
     * 音乐列表
     */
    MUSIC_HOME = '/db/music/home',

    /**
     * 音乐喜欢
     */
    MUSIC_LIKE = '/db/music/like',

    /**
     * 本地设置
     */
    LOCAL_SETTING = '/local/setting/',

    /**
     * 同步设置
     */
    SYNC_SETTING = '/sync/setting',

    /**
     * 同步设置
     */
    VIDEO_PLAY_PROTOCOL_SETTING = '/sync/vide-play-protocol',

    /**
     * 当前版本
     */
    KEY_VERSION = '/key/version',

    // =========================================================================
    // ---------------------------------- 视频 ----------------------------------
    // =========================================================================

    /**
     * 视频索引（常规视频集合索引）
     */
    VIDEO = '/db/video',

    /**
     * 视频详情
     */
    VIDEO_INFO = '/db/video/info/',

    /**
     * 视频附件
     */
    VIDEO_ATTACHMENT = '/db/video/attachment/',

    /**
     * 视频季
     */
    VIDEO_SESSION = '/db/video/session/',

    /**
     * 播放记录
     */
    VIDEO_RECORD = '/db/video/record/',

    // ============================================================================
    // ---------------------------------- 哔哩哔哩 ----------------------------------
    // ============================================================================

    /**
     * 哔哩哔哩索引视频信息
     */
    BILIBILI = '/db/bilibili',

    /**
     * 哔哩哔哩基础视频信息
     */
    BILIBILI_INFO = '/db/bilibili/info/',

    // ===========================================================================
    // ---------------------------------- 关键字 ----------------------------------
    // ===========================================================================

    KEY_VIDEO_COLLAPSED = '/key/video/collapsed',

    KEY_MOVIE_COLLAPSED = '/key/movie/collapsed',

    KEY_BACKUP = '/key/backup',

    KEY_VIDEO_SETTING_SOURCE_BILIBILI = '/key/video/setting/source/bilibili'

}

export default LocalNameEnum;
