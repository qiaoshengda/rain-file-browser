enum VideoTypeEnum {

    /**
     * 电影
     */
    MOVIE = 'movie',

    /**
     * 剧集
     */
    EPISODE = 'episode',

    /**
     * 哔哩哔哩
     */
    BILIBILI = 'bilibili'

}

export default VideoTypeEnum;
