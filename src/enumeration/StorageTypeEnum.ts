enum StorageTypeEnum {

    WEBDAV = 0,

    ALIST_V3 = 1,

    GITEE = 3,

    QINIU = 4,

    /**
     * 文件浏览器
     */
    FILE_BROWSER = 5,

    /**
     * 本地文件浏览器
     */
    LOCALHOST = 6

}

export default StorageTypeEnum;
