enum SettingLayoutEnum {

    INFO = 1,

    TABLE = 2,

    /**
     * 网格布局
     */
    GRID = 3,

    /**
     * 平铺布局
     */
    TILE = 4

}

export default SettingLayoutEnum;
