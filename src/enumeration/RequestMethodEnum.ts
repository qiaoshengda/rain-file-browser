enum RequestMethodEnum {

    GET = 'GET',

    POST = 'POST',

    PUT = 'PUT',

    DELETE = 'DELETE'

}

export default RequestMethodEnum;