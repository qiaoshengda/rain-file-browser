enum SettingOrderEnum {

    /**
     * 名字正序
     */
    DEFAULT = 1,

    /**
     * 名字正序
     */
    NAME_ASC = 2,

    /**
     * 名字倒序
     */
    NAME_DESC = 3,

    /**
     * 更新时间正序
     */
    UPDATE_TIME_ASC = 4,

    /**
     * 更新时间倒序
     */
    UPDATE_TIME_DESC = 5,

    /**
     * 根据类型排序
     */
    TYPE = 6

}

export default SettingOrderEnum;