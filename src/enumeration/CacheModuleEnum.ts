enum CacheModuleEnum {

    MUSIC = 'music',

    MUSIC_COVER = 'music/cover',

    VIDEO_COVER = 'video/cover',

    BILIBILI_COVER = 'bilibili/cover'

}

export default CacheModuleEnum;
