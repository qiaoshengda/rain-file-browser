/// <reference types="vite/client" />
declare module 'xgplayer-hls.js';
declare module 'xgplayer-flv';

interface Window {
    preload: {
        fs: {
            /**
             *  下载一个文件
             * @param config axios配置项
             * @param name 本地文件名
             * @param path 本地文件所在路径
             * @param progress 进度回调
             * @returns 文件实际路径
             */
            download(config: any, name: string, path: string, progress?: (progress: number, total: number) => void): Promise<string>,
            /**
             * 判断文件是否存在
             * @param path 缓存目录
             */
            exists(path: string): Promise<boolean>
        },
        /**
         * 发送消息
         * @param id 消息ID
         * @param channel 消息通道
         * @param msg 消息内容
         */
        sendMsg(id: number, channel: string, msg: any),
        file: {
            list(base: string, path: string): Promise<Array<{
                name: string;
                path: string;
                folder: boolean;
                updateTime: Date|string;
                size: number;
            }>>;
            copyFileWrap(base: string, path: string, destination: string): Promise<void>;
            deleteBatch(base: string, items: Array<string>): Promise<void>;
        }
    }
}
