import FileListItem from "@/domain/file/FileListItem";
import Constant from "@/global/Constant";
import {useFileMoveEvent, useTextOpenEvent, useVideoCreateEvent,} from "@/global/BeanFactory";
import {useBrowserStore} from "@/store/components/BrowserStore";
import MessageBoxUtil from "@/utils/MessageBoxUtil";
import MessageUtil from "@/utils/MessageUtil";
import {useDownloadStore} from "@/store/components/DownloadStore";
import {useSyncSettingStore} from "@/store/db/SyncSettingStore";
import {useMusicHomeStore} from "@/store/db/MusicHomeStore";

export interface MenuItem {

    name: string;

    show: (item: FileListItem) => boolean;

    action: (item: FileListItem) => void;

}

export const menuItems: Array<MenuItem> = [{
    name: '以文本形式打开',
    show: item => {
        if (item.folder) {
            return false;
        }
        return Constant.musicExtra.indexOf(item.extra) === -1 &&
            Constant.videoExtra.indexOf(item.extra) === -1 &&
            Constant.officeExtra.indexOf(item.extra) === -1 &&
            useSyncSettingStore().imageExtra.indexOf(item.extra) === -1
    },
    action: item => useTextOpenEvent.emit(item)
}, {
    name: '复制外链',
    show: item => !item.folder,
    action: useBrowserStore().copyLink
}, {
    name: '下载',
    show: item => !item.folder,
    action: item => {
        if (item.folder) {
            MessageUtil.error("文件夹暂不支持下载");
            return;
        }
        useDownloadStore().add(item, useBrowserStore().driver)
    }
}, {
    name: '复制',
    show: () => true,
    action: item => useFileMoveEvent.emit({
        option: 'copy',
        item
    })
}, {
    name: '移动',
    show: () => true,
    action: item => useFileMoveEvent.emit({
        option: 'move',
        item
    })
}, {
    name: '重命名',
    show: () => true,
    action: useBrowserStore().rename
}, {
    name: '删除',
    show: () => true,
    action: item => MessageBoxUtil.confirm(
        `确认删除文件${item.folder ? '夹' : ''}【${item.name}】，删除后无法恢复`,
        '删除提示',
        {
            confirmButtonText: "删除",
            cancelButtonText: "取消"
        })
        .then(() => useBrowserStore().rm(item).then(() => console.log("删除完成")))
        .catch(() => console.debug("取消删除"))
}, {
    name: '新增电影',
    show: item => Constant.videoExtra.indexOf(item.extra) > -1,
    action: item => useVideoCreateEvent.emit(item)
}, {
    name: '新增歌曲到列表',
    show: item => Constant.musicExtra.indexOf(item.extra) > -1,
    action: item => useMusicHomeStore().add(item, useBrowserStore().storage.id)
        .then(() => MessageUtil.success("新增成功"))
        .catch(e => MessageUtil.error("新增失败", e))
}, {
    name: '复制地址路径',
    show: () => true,
    action: item => {
        utools.copyText(item.path);
        MessageUtil.success("已复制到剪切板");
    }
}]
