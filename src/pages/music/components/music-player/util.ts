/**
 * 渲染时间
 * @param time 时间，秒
 */
export function renderTime(time: number): string {
    const min = Math.floor(time / 60).toString().padStart(2, "0");
    const second = (time % 60).toFixed(0).padStart(2, "0");
    return `${min}:${second}`;
}

