import FileListItem from "@/domain/file/FileListItem";
import {TreeNodeData} from "@arco-design/web-vue";
import Constant from "@/global/Constant";
import {h} from "vue";
import FileIcon from "@/components/icon/FileIcon.vue";

export interface FileItem {
    storageId: number;
    storageName: string;
    path: string
}


export function renderTreeNodeData(
    items: Array<FileListItem>,
    storageId: number,
    signs: Set<string>
): Array<TreeNodeData> {
    return items
        .filter(e => e.folder || Constant.musicExtra.indexOf(e.extra) > -1)
        .sort((a, b) => a.name.localeCompare(b.name))
        .map(e => ({
            title: e.name,
            key: e.path,
            isLeaf: !e.folder,
            extra: e.extra,
            icon: () => ([h(FileIcon, {
                extra: e.extra,
                folder: e.folder,
                size: 12
            })]),
            disabled: handleDisabled(e.path, storageId, signs)
        }));

}

export function handleDisabled(path: string, storageId: number, signs: Set<string>): boolean {
    return signs.has(storageId + path);
}
