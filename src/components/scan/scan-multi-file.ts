import Driver from "@/module/driver/Driver";
import {getDefaultVideoInfo, getDefaultVideoRecord, Video, VideoSession} from "@/entity/Video";
import Constant from "@/global/Constant";
import FileListItem from "@/domain/file/FileListItem";
import {parseFileName, parsePathName, sleep} from "@/utils/FieldUtil";
import VideoTypeEnum from "@/enumeration/VideoTypeEnum";

/**
 * 扫描一个目录下的多个文件，组成多个电影
 * @param path 电影所在目录
 * @param driver 驱动
 * @param storageId 存储
 * @return 多个电影
 */
export async function scanMultiFile(path: string, driver: Driver, storageId: number): Promise<Array<Video>> {
    // 获取目录下全部视频
    const files = await driver.list(path);
    const videos = new Array<Video>();
    for (let file of files) {
        if (file.folder) {
            continue;
        }
        if (Constant.videoExtra.indexOf(file.extra) === -1) {
            continue;
        }
        videos.push(renderVideo(file, storageId));
        await sleep(100);
    }
    if (videos.length === 0) {
        return Promise.reject("文件夹下不存在视频文件，未解析到视频");
    }
    return Promise.resolve(videos);
}

function renderVideo(file: FileListItem, storageId: number): Video {
    const now = new Date();
    const name = parseFileName(file.name);
    const sessions: Array<VideoSession> = [{
        id: now.getTime() + 50,
        name: "Session 1",
        items: [{
            id: now.getTime() + 75,
            name: name,
            path: file.path
        }]
    }];
    return {
        sessions,
        record: getDefaultVideoRecord(sessions),
        index: {
            id: now.getTime() + 100,
            createTime: now,
            updateTime: now,
            name: name,
            cover: '',
            type: VideoTypeEnum.MOVIE,
            storageId: storageId,
            localCover: false
        },
        info: getDefaultVideoInfo()
    }
}
