import Driver from "@/module/driver/Driver";
import {Bilibili} from "@/entity/bilibili";
import Constant from "@/global/Constant";

/**
 * 扫描多个记录成为哔哩哔哩视频
 *
 * @param path 视频所在路径
 * @param driver 驱动
 * @param storageId 存储ID
 */
export async function scanMultiToBilibili(path: string, driver: Driver, storageId: number): Promise<Array<Bilibili>> {
    const files = await driver.list(path);
    const items = new Array<Bilibili>();
    for (let file of files) {
        if (file.folder) {
            continue;
        }
        if (Constant.videoExtra.indexOf(file.extra) === -1) {
            continue;
        }
        // 视频文件
        items.push({
            index: {
                id: 0,
                createTime: '',
                updateTime: '',
                love: false,
                name: file.name,
                bid: '',
                cover: '',
                storageId: storageId,
                path: file.path,
                localCover: false
            },
            info: {
                description: '',
                tags: [],
                publishTime: '',
                upNickname: '',
                upSpaceId: '',
                extra: file.extra
            }
        });
    }
    return Promise.resolve(items);
}
