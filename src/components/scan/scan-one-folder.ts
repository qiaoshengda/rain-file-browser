import Driver from "@/module/driver/Driver";
import {getDefaultVideoInfo, getDefaultVideoRecord, Video, VideoSession} from "@/entity/Video";
import FileListItem from "@/domain/file/FileListItem";
import {parseFileName, parsePathName, sleep} from "@/utils/FieldUtil";
import VideoTypeEnum from "@/enumeration/VideoTypeEnum";
import Constant from "@/global/Constant";

/**
 * 扫描一个文件夹作为一个剧集
 * @param path 当前文件夹路径
 * @param driver 当前使用的驱动
 * @param storageId 存储ID
 */
export async function scanOneFolder(path: string, driver: Driver, storageId: number): Promise<Video> {
    const folders = await driver.list(path);
    const sessions = new Array<VideoSession>();
    const files = new Array<FileListItem>()
    for (let folder of folders) {
        if (folder.folder) {
            const session = renderSession(await driver.list(folder.path), sessions.length, folder);
            if (session.items.length > 0) {
                sessions.push(session);
            }
            await sleep(100);
        } else {
            files.push(folder);
        }
    }
    if (files.length > 0) {
        const session = renderSession(files, sessions.length);
        if (session.items.length > 0) {
            sessions.push(session);
        }
    }
    const now = new Date();
    if (sessions.length === 0) {
        return Promise.reject("没有解析到视频")
    }
    return Promise.resolve({
        sessions,
        record: getDefaultVideoRecord(sessions),
        index: {
            id: now.getTime() + 100,
            createTime: now,
            updateTime: now,
            name: parsePathName(path),
            cover: '',
            type: VideoTypeEnum.EPISODE,
            storageId: storageId,
            localCover: false
        },
        info: getDefaultVideoInfo()
    });
}


function renderSession(files: Array<FileListItem>, len: number, folder?: FileListItem): VideoSession {
    let index = 0;
    const now = new Date().getTime();
    return {
        id: now + len,
        name: folder ? folder.name : 'Session' + (len + 1),
        items: files
            // 只匹配视频
            .filter(e => Constant.videoExtra.indexOf(e.extra) > -1)
            // 按照名称排序
            .sort((a, b) => a.name.localeCompare(b.name))
            .map(e => {
                index += 1;
                return {
                    id: now + index + len,
                    name: parseFileName(e.name),
                    path: e.path
                }
            })
    }
}
