import LocalNameEnum from "@/enumeration/LocalNameEnum";
import Constant from "@/global/Constant";
import MessageUtil from "@/utils/MessageUtil";
import {getCurrentStoragePrefix, useStorageStore} from "@/store/db/StorageStore";
import {sleep} from "@/utils/FieldUtil";
import NotificationUtil from "@/utils/NotificationUtil";
import {useSyncSettingStore} from "@/store/db/SyncSettingStore";
import SyncSetting from "@/entity/setting/SyncSetting";
import {toRaw} from "vue";
import {useVideoPlayProtocolStore} from "@/store/db/VideoPlayProtocolStore";
import StorageTypeEnum from "@/enumeration/StorageTypeEnum";

export default function updateCheck(toUpdate: () => void) {
    utools.db.promises.get(LocalNameEnum.KEY_VERSION)
        .then(res => {
            if (res) {
                if (res.value !== Constant.version) {
                    // 更新
                    MessageUtil.success(`欢迎您更新到【${Constant.version}】`);
                    utools.db.promises.put({
                        _id: LocalNameEnum.KEY_VERSION,
                        _rev: res._rev,
                        value: Constant.version
                    });
                    toUpdate();
                    if (Constant.version === '1.3.2') {
                        updateTo132();
                    } else if (Constant.version === '1.3.5') {
                        updateTo135();
                    }
                }
            } else {
                // 第一次
                MessageUtil.success("欢迎您使用听雨文件浏览器");
                utools.db.promises.put({
                    _id: LocalNameEnum.KEY_VERSION,
                    value: Constant.version
                });
                utools.db.promises.allDocs("storage")
                    .then(async items => {
                        for (let item of items) {
                            await useStorageStore().add(item.value);
                            await sleep(100);
                        }
                    });
                if (Constant.version === '1.2.0') {
                    NotificationUtil.info(
                        "此次版本更新项目进行了重构，导致存储类型异常，请修改每一个存储的存储类型！",
                        "1.2.0更新提示")
                }
            }
        })
}

function updateTo132() {
    // 修改设置
    const videoDefaultPlay = useSyncSettingStore().videoDefaultPlay ? -1 : 0;
    const instance: SyncSetting = {
        ...toRaw(useSyncSettingStore().instance),
        videoDefaultPlay
    }

    Promise.all([
        useSyncSettingStore().save(instance),
        useVideoPlayProtocolStore().add({
            name: "PotPlayer",
            template: "potplayer://{url}"
        })
    ])
        .then(() => MessageUtil.success("版本更新后数据更新成功"))
        .catch(e => MessageUtil.error("版本更新后数据更新失败，请自行在同步设置中设置【视频默认播放方式】。", e));
}

function updateTo135() {

    useStorageStore().storages.forEach(storage => {
        const prefix = getCurrentStoragePrefix(StorageTypeEnum.WEBDAV, storage.id);
        console.log(prefix)
        utools.db.promises.allDocs(prefix).then(items => {
            console.log(items)
            items.forEach(item => {
                const id = item._id.replace("/0/", "/1/");
                console.log(item, {
                    _id: id,
                    value: item.value
                })
                utools.db.promises.put({
                    _id: id,
                    value: item.value
                })
            })
        })
    })

}
