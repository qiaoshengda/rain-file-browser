import axios from "axios";
import {toDateString} from "xe-utils";
import {BilibiliWrap} from "@/entity/bilibili";

/**
 * 爬取B站视频基本信息
 *
 * @param id BID、AVID
 * @see https://api.bilibili.com/x/web-interface/view/detail?bvid=1Vr4y1d7ax
 */
export async function crawlingBilibiliInfo(id: string): Promise<BilibiliWrap> {
    let bvid = undefined;
    let aid = undefined;
    if (id.toUpperCase().startsWith("BV")) {
        bvid = id.substring(2);
    } else {
        aid = id;
    }
    const rsp = await axios<any>({
        url: "https://api.bilibili.com/x/web-interface/view/detail",
        params: {
            bvid,
            aid
        },
        method: 'GET'
    })
    const result = rsp.data;
    const code = result.code;
    if (code === -400) {
        return Promise.reject("请求错误");
    } else if (code === -403) {
        return Promise.reject("权限不足");
    } else if (code === -404) {
        return Promise.reject("无视频");
    } else if (code === 62002) {
        return Promise.reject("稿件不可见");
    } else if (code === 62004) {
        return Promise.reject("稿件审核中");
    }
    const data = result.data;
    return Promise.resolve({
        index: {
            name: data['View'].title,
            cover: data['View'].pic,
            bid: id
        },
        info: {
            description: data['View'].desc,
            upNickname: data['Card'].card.name,
            upSpaceId: data['Card'].card.mid,
            publishTime: toDateString(new Date(data['View'].pubdate * 1000)),
            tags: data['Tags'].map((e: any) => e.tag_name)
        }
    });
}
