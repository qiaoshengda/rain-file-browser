import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia';
// 额外引入图标库
import ArcoVueIcon from '@arco-design/web-vue/es/icon';
import ArcoVue from '@arco-design/web-vue';
import router from '@/plugins/router';

// 引入样式
import '@/less/layout.less'
import '@arco-design/web-vue/dist/arco.css';
import '@vue-office/docx/lib/index.css'
import '@vue-office/excel/lib/index.css'
import 'APlayer/dist/APlayer.min.css';

createApp(App)
    .use(ArcoVue)
    .use(ArcoVueIcon)
    .use(createPinia())
    .use(router)
    .mount('#app')
