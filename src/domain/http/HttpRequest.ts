import HttpProgressEvent from "./HttpProgressEvent";

export default interface HttpRequest<D = any> {

    baseURL?: string;

    url: string;

    method?: 'get' | 'GET'
        | 'delete' | 'DELETE'
        | 'head' | 'HEAD'
        | 'options' | 'OPTIONS'
        | 'post' | 'POST'
        | 'put' | 'PUT'
        | 'patch' | 'PATCH'
        | 'purge' | 'PURGE'
        | 'link' | 'LINK'
        | 'unlink' | 'UNLINK';

    timeout?: number;

    responseType?: 'text' | 'json' | 'arraybuffer' | 'blob';

    data?: D;

    headers?: Record<string, string>;

    params?: Record<string, any>;

    auth?: {

        username: string;

        password: string;

    }

    userAgent?: string;

    /**
     * 编码
     */
    charset?: string;

    /**
     * 上传进程回调
     * @param progressEvent 进程事件
     */
    onUploadProgress?: (progressEvent: HttpProgressEvent) => void;


    /**
     * 下载进程回调
     * @param progressEvent 进程事件
     */
    onDownloadProgress?: (progressEvent: HttpProgressEvent) => void;
}

