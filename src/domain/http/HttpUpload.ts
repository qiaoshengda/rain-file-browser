import FileListItemSearch from "@/domain/file/FileListItemSearch";

export interface UploadResult {
    abort: () => void;
}

export interface UploadCallback {
    onProgress: (current: number) => void;
    onSuccess: () => void;
    onError: (e: any) => void;
}

export interface SearchCallback {
    onProgress: (items: Array<FileListItemSearch>) => void;
    onSuccess: () => void;
    onError: (e: any) => void;
}
