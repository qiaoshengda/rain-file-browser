import HttpRequest from "./HttpRequest";

export default interface HttpResponse<T = string, D = any> {

    /**
     * 响应数据
     */
    data: T;

    status: number;

    statusText: string;

    headers: Record<string, string>;

    /**
     * 请求内容
     */
    config: HttpRequest<D>;

}
