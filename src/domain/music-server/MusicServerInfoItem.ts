export interface MusicServerInfoItem {

    url: string;

    cover: string;

    lyrics?: string

}
