export interface MusicServerListItem {

    /**
     * 唯一标识
     */
    id: string;

    /**
     * 封面
     */
    cover: string;

    /**
     * 曲名
     */
    title: string;

    /**
     * 专辑
     */
    album: string;

    /**
     * 歌手
     */
    artist: string;

    /**
     * 后缀名、品质
     */
    suffix: string;

    /**
     * 时长（秒）
     */
    duration: number;

    /**
     * 链接
     */
    url:string;

}
