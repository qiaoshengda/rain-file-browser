import {VideoItem} from "@/entity/Video";

export default interface VideoPlayItem {

    /**
     * 播放的ID
     */
    id: number;

    /**
     * 列表
     */
    items: Array<VideoItem>;

}
