import {MusicItem} from "@/entity/MusicList";

export default interface MusicPlayItem {

    /**
     * 当前播放第几个
     */
    index: number;

    /**
     * 音乐项
     */
    items: Array<MusicItem>;

}
