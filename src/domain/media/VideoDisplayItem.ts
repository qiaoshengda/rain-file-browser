export default interface VideoDisplayItem {

    id: number;

    createTime: Date | string;

    updateTime: Date | string;

    /**
     * 视频名称
     */
    name: string;

    /**
     * 视频的封面
     */
    cover: string;

    /**
     * 本地封面是否存在
     */
    localCover: boolean;

}
