import FileListItem from "@/domain/file/FileListItem";

export default interface FileListItemSearch extends FileListItem {

    /**
     * 所在目录
     */
    parent: string;

}
