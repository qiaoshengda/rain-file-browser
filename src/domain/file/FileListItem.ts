export default interface FileListItem {

    /**
     * 名字
     */
    name: string;

    /**
     * 拓展名
     */
    extra: string;

    /**
     * 是否是文件夹
     */
    folder: boolean;

    /**
     * 路径
     */
    path: string;

    /**
     * 更新时间
     */
    updateTime: string | Date | null;

    /**
     * 大小
     */
    size: number;

    /**
     * 封面，只有图片/视频有
     */
    cover: string;

    /**
     * 描述
     */
    description?: string;

    /**
     * 拓展信息
     */
    expands?: Record<string, string>;

}
