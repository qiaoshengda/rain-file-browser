import FileListItem from "@/domain/file/FileListItem";

export default interface FileListItemWrap extends FileListItem {

    /**
     * 是否是快速上传
     */
    quickUpload: boolean;

}
