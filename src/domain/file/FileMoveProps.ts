import FileListItem from "@/domain/file/FileListItem";

export default interface FileMoveProps {

    /**
     * 操作
     */
    option: 'move' | 'copy';

    /**
     * 文件项
     */
    item: FileListItem;

}
