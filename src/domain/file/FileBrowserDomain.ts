// ------------------------- 文件项 -------------------------

export interface FileBrowserResource {
    items: Item[];
    numDirs: number;
    numFiles: number;
    sorting: Sorting;
    path: string;
    name: string;
    size: number;
    extension: string;
    modified: string;
    mode: number;
    isDir: boolean;
    isSymlink: boolean;
    type: string;
}

export interface Sorting {
    by: string;
    asc: boolean;
}

export interface Item {
    path: string;
    name: string;
    size: number;
    extension: string;
    modified: string;
    mode: number;
    isDir: boolean;
    isSymlink: boolean;
    type: string;
    content: string
}

// ------------------------- 搜索项 -------------------------

export interface SearchItem {
    dir: boolean;
    path: string;
}
