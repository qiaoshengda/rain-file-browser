export default interface FileSearch {

    /**
     * 所在目录
     */
    parent: string;

    /**
     * 搜索关键字
     */
    keyword: string;

    /**
     * 是否包含目录
     */
    dir: 'all' | 'only' | 'exclude';

    /**
     * 文件类型，存在则不包含目录
     */
    extras: Array<string>

}
