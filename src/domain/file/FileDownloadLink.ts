export default interface FileDownloadLink {

    /**
     * 主机
     */
    host: string;

    /**
     * 路径
     */
    path: string;

}
