export default interface PageResult<T> {

    /**
     * 页码
     */
    num: number;

    /**
     * 每页大小
     */
    size: number;

    /**
     * 总数
     */
    total: number;

    /**
     * 记录
     */
    records: Array<T>

}
