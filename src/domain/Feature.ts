export default interface Feature {
    code: string,
    explain: string,
    platform: platform | Array<platform>,
    icon?: string,
    cmds: Array<string | Cmd>
}

type platform = 'darwin' | 'win32' | 'linux';

interface Cmd {
    type: CmdType,
    label: string,
    [key: string]: any
}

type CmdType = 'img' | 'files' | 'regex' | 'over' | 'window';
