import {IPlayerOptions} from "xgplayer";
import {WebStorage} from "@/entity/WebStorage";
import StorageTypeEnum from "@/enumeration/StorageTypeEnum";

/**
 * 美化数据单位
 *
 * @param {number} value 需要美化的值
 */
export function prettyDataUnit(value: number) {
    let gb = 1024 * 1024 * 1024.0;
    if (value > gb) {
        let temp = value / gb;
        return temp.toFixed(2) + 'GB';
    }
    let mb = 1024 * 1024.0;
    if (value > mb) {
        let temp = value / mb;
        return temp.toFixed(2) + 'MB';
    }
    let b = 1024.0;
    if (value > b) {
        let temp = value / b;
        return temp.toFixed(2) + 'KB';
    }
    return value + 'B';

}

export function prettyDate(date?: number | string | Date) {
    const now = new Date().getTime()
    const old = date ? new Date(date).getTime() : new Date().getTime()
    const diffValue = now - old;
    let result: string
    const minute = 1000 * 60
    const hour = minute * 60
    const day = hour * 24
    const month = day * 30
    const year = month * 12

    const _year = diffValue / year
    const _month = diffValue / month
    const _week = diffValue / (7 * day)
    const _day = diffValue / day
    const _hour = diffValue / hour
    const _min = diffValue / minute

    if (_year >= 1) result = _year.toFixed(0) + "年前"
    else if (_month >= 1) result = _month.toFixed(0) + "个月前"
    else if (_week >= 1) result = _week.toFixed(0) + "周前"
    else if (_day >= 1) result = _day.toFixed(0) + "天前"
    else if (_hour >= 1) result = _hour.toFixed(0) + "个小时前"
    else if (_min >= 1) result = _min.toFixed(0) + "分钟前"
    else result = "刚刚"
    return result
}

/**
 * 解析文件拓展名
 * @param name 文件名
 * @param folder 是否文件夹
 */
export function parseFileExtra(name: string, folder: boolean = false): string {
    if (folder) {
        return 'folder'
    }
    let extraIndex = name.lastIndexOf('.');
    if (extraIndex > -1) {
        return name.substring(extraIndex + 1);
    }
    return '';
}

/**
 * 解析文件名
 * @param name 文件名
 */
export function parseFileName(name: string): string {
    let extraIndex = name.lastIndexOf('.');
    if (extraIndex > -1) {
        return name.substring(0, extraIndex);
    }
    return name;
}

/**
 * 解析文件名
 * @param path 文件名
 */
export function parsePathName(path: string): string {
    let extraIndex = path.lastIndexOf('/');
    if (extraIndex > -1) {
        return path.substring(extraIndex + 1);
    }
    return path;
}


export function sleep(time: number): Promise<void> {
    return new Promise<void>(resolve => setTimeout(resolve, time));
}

export function getDefaultVideoConfig(id: string, url?: string): IPlayerOptions {
    return {
        id,
        url: url || '',
        playsinline: true,
        width: "100%",
        height: "100%",
        autoplay: true,
        fluid: false,
        playbackRate: [
            0.5,
            1,
            2,
            3
        ],
        screenShot: {
            hideButton: false,
            iconText: '截屏'
        },
        download: true,
        rotate: {   //视频旋转按钮配置项
            innerRotate: true, //只旋转内部video
            clockwise: false // 旋转方向是否为顺时针
        }
    }
}

export function pathJoin(...path: string[]): string {
    return path.join(utools.isWindows() ? '\\' : '/')
}

export function getUrlByWebStorage(storage: WebStorage): string {
    switch (storage.type) {
        case StorageTypeEnum.ALIST_V3:
            return storage.alistV3.url;
        case StorageTypeEnum.FILE_BROWSER:
            return storage.fileBrowser.url;
        case StorageTypeEnum.WEBDAV:
            return storage.webdav.url;
        case StorageTypeEnum.GITEE:
            return storage.gitee.repo;
        case StorageTypeEnum.QINIU:
            return storage.qiniu.host;
        default:
            return "";
    }
}
