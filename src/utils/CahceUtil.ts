import VideoDisplayItem from "@/domain/media/VideoDisplayItem";
import CacheModuleEnum from "@/enumeration/CacheModuleEnum";
import {parseUrl} from "xe-utils";
import {useLocalSettingStore} from "@/store/db/LocalSettingStore";
import {parsePathName, pathJoin} from "@/utils/FieldUtil";

/**
 * 缓存视频封面
 *
 * @param item 视频
 * @param module 缓存模块
 */
export function cacheVideoCover(item: VideoDisplayItem, module: CacheModuleEnum): Promise<boolean> {
    return cacheItem({
        id: item.id,
        url: item.cover
    }, module)
}

/**
 * 渲染视频封面缓存
 * @param item 视频
 * @param module 缓存模块
 */
export function renderVideoCover(item: VideoDisplayItem, module: CacheModuleEnum): string {
    return renderItem({
        id: item.id,
        url: item.cover
    }, module)
}

export interface CacheItem {

    id: number;

    url: string;

}

/**
 * 缓存一项
 * @param item 缓存项
 * @param module 缓存模块
 * @return 是否进行缓存
 */
export async function cacheItem(item: CacheItem, module: CacheModuleEnum): Promise<boolean> {
    if (item.url.trim() !== '') {
        // 存在封面
        const pathname = parseUrl(item.url).pathname;
        const exist = await window.preload.fs.exists(pathJoin(useLocalSettingStore().cachePath, module, item.id + '-' + parsePathName(pathname)))
        if (!exist) {
            // 不存在，缓存
            console.log("不存在缓存，开始缓存")
            await window.preload.fs.download({
                url: item.url,
                method: 'GET'
            }, item.id + '-' + parsePathName(pathname), pathJoin(useLocalSettingStore().cachePath, module))
            console.log("缓存成功");
            return Promise.resolve(true);
        }
    }

    return Promise.resolve(false);
}

export function renderItem(item: CacheItem, module: CacheModuleEnum): string {
    if (item.url.trim() !== '') {
        const pathname = parseUrl(item.url).pathname;
        return pathJoin(useLocalSettingStore().cachePath, module, item.id + '-' + parsePathName(pathname));
    } else {
        return 'icon/404.png';
    }
}

export async function hasExistCache(item: CacheItem, module: CacheModuleEnum): Promise<boolean> {
    if (item.url.trim() !== '') {
        const pathname = parseUrl(item.url).pathname;
        return await window.preload.fs.exists(pathJoin(useLocalSettingStore().cachePath, module, item.id + '-' + parsePathName(pathname)))
    } else {
        return Promise.resolve(false);
    }
}
