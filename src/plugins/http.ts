import HttpRequest from "@/domain/http/HttpRequest";
import HttpResponse from "@/domain/http/HttpResponse";
import {useSyncSettingStore} from "@/store/db/SyncSettingStore";
import axios from "axios";

const instance = axios.create({
    timeout: 5000
});

export default function http<T = string, D = any>(config: HttpRequest<D>): Promise<HttpResponse<T>> {
    return new Promise<HttpResponse<T>>((resolve, reject) => {
        if (!config.responseType) {
            config.responseType = "json";
        }
        if (!config.timeout) {
            // 超时时间
            config.timeout = useSyncSettingStore().timeout;
        }
        instance(config).then(response => {
            resolve(response as any);
        }).catch((e: any) => reject(e));
    });
};
