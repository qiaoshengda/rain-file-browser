import {RouteRecordRaw} from "vue-router";

export const MusicServerRouters: Array<RouteRecordRaw> = [{
    name: '音乐服务器-主页',
    path: '/music-server/home/:id',
    component: () => import('@/pages/music-server/home/index.vue')
}, {
    name: '音乐服务器-编辑',
    path: '/music-server/edit/:id',
    component: () => import("@/pages/music-server/edit/index.vue")
}]
