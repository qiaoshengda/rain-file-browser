import {RouteRecordRaw} from "vue-router";

export const SettingRouters: RouteRecordRaw[] = [{
    name: "同步设置",
    path: '/setting/sync',
    component: () => import('@/pages/settings/sync/index.vue')
}, {
    name: "本地设置",
    path: '/setting/local',
    component: () => import('@/pages/settings/local/index.vue')
}, {
    name: "备份",
    path: '/setting/backup',
    component: () => import('@/pages/settings/backup/index.vue')
}, {
    name: "视频播放协议",
    path: '/setting/video-play-protocol',
    component: () => import('@/pages/settings/video-play-protocol/index.vue')
}, {
    name: "快速上传",
    path: '/setting/upload',
    component: () => import('@/pages/settings/upload/index.vue')
}];
