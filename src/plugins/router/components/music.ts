import {RouteRecordRaw} from "vue-router";

export const MusicRouters: RouteRecordRaw = {
    name: "音乐",
    path: '/music',
    redirect: '/music/home',
    component: () => import('@/pages/music/index.vue'),
    children: [{
        name: '歌曲列表',
        path: 'home',
        component: () => import('@/pages/music/components/home.vue')
    }, {
        name: '歌单',
        path: 'list',
        component: () => import('@/pages/music/components/list.vue')
    }, {
        name: "歌单详情",
        path: 'list-item/:id',
        component: () => import('@/pages/music/components/list-item.vue')
    }, {
        name: '扫描',
        path: 'scan',
        component: () => import('@/pages/music/components/scan/index.vue')
    }]
}
