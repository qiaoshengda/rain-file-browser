import {RouteRecordRaw} from "vue-router";

export const MovieRouters: RouteRecordRaw = {
    name: "影视",
    path: '/movie/:id',
    component: () => import('@/pages/movie/index.vue'),
    children: [{
        name: '主页',
        path: 'home',
        component: () => import('@/pages/movie/home/index.vue')
    }]
}
