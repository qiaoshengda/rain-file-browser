import {RouteRecordRaw} from "vue-router";

export const VideoRouters: RouteRecordRaw = {
    name: "视频",
    path: '/video',
    redirect: '/video/all',
    component: () => import('@/pages/video/index.vue'),
    children: [{
        name: '全部视频',
        path: 'all',
        component: () => import('@/pages/video/components/video-list.vue')
    }, {
        name: '电影',
        path: 'movie',
        component: () => import('@/pages/video/components/video-list.vue')
    }, {
        name: '电视剧',
        path: 'tv',
        component: () => import('@/pages/video/components/video-list.vue')
    }, {
        name: "视频详情",
        path: 'item/:id',
        component: () => import('@/pages/video/info/index.vue')
    }]
}
