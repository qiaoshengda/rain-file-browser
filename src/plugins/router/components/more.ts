import {RouteRecordRaw} from "vue-router";

export const MoreRouters: RouteRecordRaw[] = [{
    name: "推荐",
    path: '/more/recommend',
    component: () => import('@/pages/more/recommend/index.vue')
}, {
    name: "帮助",
    path: '/more/help',
    component: () => import('@/pages/more/help/index.vue')
}, {
    name: "更新日志",
    path: '/more/update',
    component: () => import('@/pages/more/update/index.vue')
}];
