import {RouteRecordRaw} from "vue-router";

export const BilibiliRouters: RouteRecordRaw = {
    name: "哔哩哔哩",
    path: '/bilibili',
    redirect: '/bilibili/list',
    component: () => import('@/pages/bilibili/index.vue'),
    children: [{
        name: '哔哩哔哩',
        path: 'list',
        component: () => import('@/pages/bilibili/list/index.vue')
    }, {
        name: "哔哩哔哩视频详情",
        path: 'info/:id',
        component: () => import('@/pages/bilibili/info/index.vue')
    }, {
        name: "设置",
        path: 'setting',
        component: () => import('@/pages/bilibili/setting/index.vue')
    }]
}
