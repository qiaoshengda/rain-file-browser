import {createRouter, createWebHashHistory} from 'vue-router';
import {MusicRouters} from "@/plugins/router/components/music";
import {VideoRouters} from "@/plugins/router/components/video";
import {BilibiliRouters} from "@/plugins/router/components/bilibili";
import {MoreRouters} from "@/plugins/router/components/more";
import {SettingRouters} from "@/plugins/router/components/setting";
import {MovieRouters} from "@/plugins/router/components/movie";
import {MusicServerRouters} from "@/plugins/router/components/music-server";
// 引入路由

const router = createRouter({
    history: createWebHashHistory(),
    routes: [{
        name: "首页",
        path: '/',
        alias: '/home',
        component: () => import('@/pages/home/index.vue'),
    }, {
        name: "编辑存储",
        path: '/storage/:id',
        component: () => import('@/pages/storage/edit.vue')
    }, {
        name: "浏览",
        path: '/browser/:id',
        component: () => import('@/pages/browser/index.vue')
    }, {
        name: '影视编辑',
        path: '/movie/edit/:id',
        component: () => import("@/pages/movie/edit/index.vue")
    },
        MusicRouters, VideoRouters, BilibiliRouters, MovieRouters, ...MoreRouters, ...SettingRouters,
        ...MusicServerRouters]
});

export default router;
