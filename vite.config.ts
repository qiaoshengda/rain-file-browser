// vite.config.js
import vue from "@vitejs/plugin-vue";
import { defineConfig, PluginOption } from "vite";
import path from "path";
import electron from 'vite-electron-plugin';
import { alias } from 'vite-electron-plugin/plugin';

function _resolve(dir: string) {
    return path.resolve(__dirname, dir);
}

export default defineConfig({
    resolve: {
        alias: {
            "@": _resolve("src")
        },
    },
    plugins: [vue()],
    base: "./",
    build: {
        outDir: 'src-utools/dist'
    }
});
