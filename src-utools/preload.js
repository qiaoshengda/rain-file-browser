const {ipcRenderer} = require('electron');

"use strict";
window.preload = Object.create(null);

// 导出fs工具类
try {
    window.preload['fs'] = require('./util/FsUtil');
} catch (e) {
    console.error(e);
    if (window.utools) {
        utools.showNotification(e.message);
    }
}

// 导出文件操作工具类
try {
    window.preload['file'] = require('./util/LocalhostDriver');
} catch (e) {
    console.error(e);
    if (window.utools) {
        utools.showNotification(e.message);
    }
}

/**
 * 发送消息
 * @param {number} id 消息ID
 * @param {string} channel 消息通道
 * @param {boolean} msg 消息内容
 */
window.preload["sendMsg"] = function (id, channel, msg) {
    ipcRenderer.sendTo(id, channel, msg)
}
