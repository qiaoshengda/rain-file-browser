const {
    readdir,
    statSync,
    copyFile,
    unlinkSync,
    readdirSync
} = require('fs');
const {join, sep} = require('path');

/**
 * 读取文件列表
 *
 * @param {string} base 基础目录
 * @param {string} path 文件目录
 * @return {Promise<Array<{
 *     name: string;
 *     path: string;
 *     folder: boolean;
 *     updateTime: Date|string;
 *     size: string;
 * }>>}
 */
function list(base, path) {
    // 实际的路径
    const current = join(base, path.replace('/', sep));
    return new Promise((resolve, reject) => {
        readdir(current, (e, names) => {
            if (e) {
                reject(e);
                return;
            }
            resolve(names.map(name => {
                try {
                    const stat = statSync(join(current, name));
                    return {
                        name: name,
                        path: (path === '/' ? '' : path) + '/' + name,
                        folder: stat.isDirectory(),
                        updateTime: stat.mdate,
                        size: stat.size
                    }
                } catch (e) {
                    console.error(e);
                    return null;
                }
            }).filter(e => e !== null));
        })
    })
}

/**
 * 拷贝文件
 *
 * @param {string} base 基础目录
 * @param {string} path 原始文件路径
 * @param {string} destination 目标文件路径
 * @return {Promise<void>}
 */
function copyFileWrap(base, path, destination) {
    // 实际的路径
    const source = join(base, path.replace('/', sep));
    const target = join(base, destination.replace('/', sep));

    return new Promise((resolve, reject) => {
        copyFile(source, target, err => {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        })
    })
}

/**
 * 批量删除
 * @param {string} base 基础目录
 * @param {Array<string>} items 要删除的目录
 * @return {Promise<void>}
 */
function deleteBatch(base, items) {
    return new Promise((resolve, reject) => {
        try {
            items.map(item => join(base, item.replace('/', sep)))
                .forEach(path => {
                    if (isDirectory(path)) {
                        deleteDir(path)
                    } else {
                        unlinkSync(path)
                    }
                });
            resolve()
        } catch (e) {
            reject(e);
        }
    })
}

/**
 * 递归删除目录
 *
 * @param {string} path 目录路径
 * @return {void}
 */
function deleteDir(path) {
    readdirSync(path).forEach(item => {
            const itemPath = join(path, item);
            if (isDirectory(itemPath)) {
                deleteDir(itemPath);
            } else {
                unlinkSync(itemPath);
            }
        }
    )
}

/**
 * 判断是否是目录
 *
 * @param {string} path 地址
 * @return {boolean} 是否是目录
 */
function isDirectory(path) {
    return statSync(path).isDirectory();
}


module.exports = {list, copyFileWrap}
