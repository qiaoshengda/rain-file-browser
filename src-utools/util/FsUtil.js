/**
 * 文件工具类
 */

const axios = require('../js/axios');
const fs = require('fs');
const {join} = require('path');

/**
 * 渲染目录
 * @param {string} path 路径
 * @param {string} name 名字
 */
function renderPath(path, name) {
    let prefix = name;
    let suffix = '';
    let extraIndex = name.lastIndexOf('.');
    if (extraIndex > -1) {
        prefix = name.substring(0, extraIndex);
        suffix = name.substring(extraIndex);
    }

    let index = 0;
    while (fs.existsSync(join(path, `${prefix}-${index}${suffix}`))) {
        index += 1;
    }
    return join(path, `${prefix}-${index}${suffix}`);
}

/**
 *  下载一个文件
 * @param {Object} config axios配置项
 * @param {string} name 本地文件名
 * @param {string} path 本地文件所在路径
 * @param {Function} progress 进度回调
 */
function download(config, name, path, progress) {
    if (!fs.existsSync(path)) {
        // 目录不存在则创建
        fs.mkdirSync(path, {
            recursive: true
        });
    }
    let re = join(path, name);
    if (fs.existsSync(re)) {
        re = renderPath(path, name);
    }
    return new Promise((resolve, reject) => {
        // 设置相应类型是流
        config.responseType = "stream";
        // 监听下载
        config.onDownloadProgress = (progressEvent) => {
            if (progress) {
                progress(progressEvent.rate || 0, progressEvent.total || 1);
            }
        }
        axios(config).then(response => {
            const writer = fs.createWriteStream(re);
            response.data.pipe(writer);
            writer.on("finish", () => resolve(re));
            writer.on("error", reject);
        }).catch(e => reject(e));
    })
}

/**
 * 判断文件是否存在
 * @param {string} path 缓存目录
 * @return {Promise<boolean>} 文件是否存在
 */
function exists(path) {
    return new Promise(resolve => {
        fs.exists(path, resolve)
    })
}


module.exports = {download, exists}
