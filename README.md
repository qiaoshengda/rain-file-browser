# 听雨影音库

在线管理Alist、WebDAV等云存储。通过本插件可以利用这些云存储建立自己的影视库，在线观看自己的影片。
也可以搭建自己的音乐库，拜托音乐软件的烦恼，在线听自己网盘中的音乐。
本插件还可以获取B站视频的元数据，将您下载的B站视频组成海报墙。

## 存储

可以在线管理自己的云储存，实现增删改查、上传、下载。

**支持的云存储**

- ✅ WebDAV
- ✅ Alist V3
- ✅ File Browser
- ✅ Gitee（测试中）
- ❌ 七牛云
- ❌ MinIO
- ❌ S3 Browser

## 影视

视频分为两种，第一种就是普通的集合类视频，就是电影和电视剧，他们一部是组视频的合集。第二种是想B站视频这样，每个视频相互独立，一个视频就是一个实体

对于普通视频，目前不能自动获取视频元数据，可以自行编辑数据

对于B站视频，输入`BVID`或`AID`编号，即可获取视频数据，推荐使用`BVID`

### 使用方法

- 创建云存储

想要搭建自己的影视库，首先要新增一个云储存。

- 新增剧集
  - 前往电视剧文件夹

    前往电视剧文件夹，文件夹下应为电视剧的全部剧集视频，如果这个电视剧分为多季，那么文件夹下应该是文件夹，每个文件夹代表一季，季文件夹下应该是视频文件。
  - 扫描
    
    点击上方工具栏中的`扫描`，选择`创建剧集`。

- 新增电影

电影文件应该是一个单独的视频，前往电影视频所在的文件夹

## 音乐

音乐的核心是音乐列表，音乐新增的方式与视频不同，因为音乐每一个音乐都是单独的，所以无需像视频那样繁琐，但仍需要至少一个云存储。

### 歌曲列表

1. 前往音乐界面
2. 选择扫描音乐
3. 选择音乐所在的云存储和音乐所在文件夹
4. 点击`开始扫描`
5. 扫描完成后会显示扫描到了多少音乐
6. 点击`新增到歌曲列表`

### 歌单

对于不同的音乐类型，我们可以使用歌单进行区分，在听的时候就可以只听这一类的音乐

1. 点击歌单
2. 点击新增
3. 前往`歌曲列表`
4. 点击`+`进行新增音乐到歌单

## 缓存

为了加快展示，本项目对特定资源进行了缓存

- 音乐
- 视频封面
- B站视频封面

缓存位置默认在utools的`AppData`目录下的`rain-file-browser`目录下，可以千万`设置` => `同步设置`中修改缓存位置。

## 开源

本项目已开源，项目地址：[Gitee](https://gitee.com/qiaoshengda/rain-file-browser)
